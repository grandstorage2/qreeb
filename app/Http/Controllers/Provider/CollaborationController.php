<?php

namespace App\Http\Controllers\Provider;

use App\Models\Category;
use App\Models\Collaboration;
use App\Models\Company;
use App\Models\CompanySubscription;
use App\Models\Order;
use App\Models\OrderRate;
use App\Models\OrderTechDetail;
use App\Models\OrderTechRequest;
use App\Models\Provider;
use App\Models\ProviderSubscription;
use App\Models\SubCompany;
use App\Models\Technician;
use App\Models\User;
use App\Models\Warehouse;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class CollaborationController extends Controller
{
    public function index()
    {
        $companies = Collaboration::where('provider_id', provider()->provider_id)->paginate(50);
        foreach($companies as $company)
        {
            $company['orders'] = $company->orders_count(provider()->provider_id,$company->company_id);
        }

        return view('provider.collaborations.index', compact('companies'));
    }


    public function statistics($id, Request $request)
    {
        $request->merge(
            [
            'collaboration_id' => $id
            ]
        );

        $this->validate($request,
            [
                'collaboration_id' => 'required|exists:collaborations,id,provider_id,'.provider()->provider_id
            ]
        );

        $collaboration = Collaboration::find($id);


        $this_month = new Carbon('first day of this month');
        $this_year = new Carbon('first day of january this year');

        $monthly_orders = Order::raw('table orders')->where('provider_id', $collaboration->provider_id)->where('company_id', $collaboration->company_id)->where('created_at','>=', $this_month->toDateTimeString())->get();
        $monthly_orders_ids = $monthly_orders->pluck('id');

        $monthly_orders_count = $monthly_orders->count();
        $monthly_open = $monthly_orders->where('completed', 0)->count();
        $monthly_closed = $monthly_orders->where('completed', 1)->where('canceled', 0)->count();
        $monthly_canceled = $monthly_orders->where('canceled', 1)->count();
        $monthly_canceled_user = $monthly_orders->where('canceled', 1)->where('canceled_by', 'user')->count();
        $monthly_canceled_tech = $monthly_orders->where('canceled', 1)->where('canceled_by', 'tech')->count();
        $monthly_revenue = $monthly_orders->sum('order_total');

        $monthly_parts_orders = $monthly_orders->where('type','re_scheduled');
        $monthly_parts_orders_count = $monthly_parts_orders->count();
        $monthly_parts = OrderTechRequest::whereIn('order_id', $monthly_parts_orders->pluck('id'));
        $monthly_parts_count = $monthly_parts->count();
        $monthly_parts_data = $monthly_parts->select('item_id','provider_id')->get();

        $monthly_arr= [];
        foreach($monthly_parts_data as $part)
        {
            $price = DB::table($part->provider_id.'_warehouse_parts')->where('id', $part->item_id)->select('price')->first()->price;
            array_push($monthly_arr, $price);
        }
        $monthly_parts_prices = array_sum($monthly_arr);

        $monthly_rates_ids = OrderRate::whereIn('order_id', $monthly_orders_ids)->get();
        $monthly_rate_commitment = (string)round($monthly_rates_ids->pluck('commitment')->avg(),0);
        $monthly_rate_cleanliness = (string)round($monthly_rates_ids->pluck('cleanliness')->avg(),0);
        $monthly_rate_performance = (string)round($monthly_rates_ids->pluck('performance')->avg(),0);
        $monthly_rate_appearance = (string)round($monthly_rates_ids->pluck('appearance')->avg(),0);

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $yearly_orders = Order::raw('table orders')->where('provider_id', $collaboration->provider_id)->where('company_id', $collaboration->company_id)->where('created_at','>=', $this_year->toDateTimeString())->get();
        $yearly_orders_ids = $yearly_orders->pluck('id');

        $yearly_orders_count = $yearly_orders->count();
        $yearly_open = $yearly_orders->where('completed', 0)->count();
        $yearly_closed = $yearly_orders->where('completed', 1)->where('canceled', 0)->count();
        $yearly_canceled = $yearly_orders->where('canceled', 1)->count();
        $yearly_canceled_user = $yearly_orders->where('canceled', 1)->where('canceled_by', 'user')->count();
        $yearly_canceled_tech = $yearly_orders->where('canceled', 1)->where('canceled_by', 'tech')->count();
        $yearly_revenue = $yearly_orders->sum('order_total');

        $yearly_parts_orders = $yearly_orders->where('type','re_scheduled');
        $yearly_parts_orders_count = $yearly_parts_orders->count();
        $yearly_parts = OrderTechRequest::whereIn('order_id', $yearly_parts_orders->pluck('id'));
        $yearly_parts_count = $yearly_parts->count();
        $yearly_parts_data = $yearly_parts->select('item_id','provider_id')->get();

        $yearly_arr= [];
        foreach($yearly_parts_data as $part)
        {
            $price = DB::table($part->provider_id.'_warehouse_parts')->where('id', $part->item_id)->select('price')->first()->price;
            array_push($yearly_arr, $price);
        }
        $yearly_parts_prices = array_sum($yearly_arr);

        $yearly_rates_ids = OrderRate::whereIn('order_id', $yearly_orders_ids)->get();
        $yearly_rate_commitment = (string)round($yearly_rates_ids->pluck('commitment')->avg(),0);
        $yearly_rate_cleanliness = (string)round($yearly_rates_ids->pluck('cleanliness')->avg(),0);
        $yearly_rate_performance = (string)round($yearly_rates_ids->pluck('performance')->avg(),0);
        $yearly_rate_appearance = (string)round($yearly_rates_ids->pluck('appearance')->avg(),0);

        return view('provider.collaborations.statistics',
            compact('collaboration','this_month','this_year','monthly_orders_count','monthly_open','monthly_closed','monthly_canceled','monthly_canceled_user','monthly_canceled_tech'
            ,'monthly_revenue','monthly_parts_orders','monthly_parts_orders_count','monthly_parts_count','monthly_parts_prices','monthly_rate_commitment','monthly_rate_appearance','monthly_rate_cleanliness'
            ,'monthly_rate_performance','yearly_orders_count','yearly_open','yearly_closed','yearly_canceled','yearly_canceled_user','yearly_canceled_tech','yearly_revenue','yearly_parts_orders','yearly_parts_orders_count'
            ,'yearly_parts_count','yearly_parts_prices','yearly_rate_commitment','yearly_rate_cleanliness','yearly_rate_performance','yearly_rate_appearance', 'id'
            )
        );
    }

    public function date_year_orders($id, $type)
    {
        $company_id = Collaboration::where('id',$id)->select('company_id')->first()->company_id;

        $company = Company::where('id', $company_id)->select('id', 'en_name')->first();

        $subs = CompanySubscription::where('company_id', $company_id)->first()->subs;
        $cat_ids = Category::whereIn('id', unserialize($subs))->pluck('parent_id');
        $cats = Category::whereIn('id', $cat_ids)->select('id','en_name')->get();

        $this_month = new Carbon('first day of this month');
        $this_year = new Carbon('first day of january this year');

        $monthly_orders = Order::raw('table orders')->where('provider_id', provider()->provider_id)
            ->where('company_id', $company_id)->where('created_at','>=', $this_month->toDateTimeString());
        $yearly_orders = Order::raw('table orders')->where('provider_id', provider()->provider_id)
            ->where('company_id', $company_id)->where('created_at','>=', $this_year->toDateTimeString());

        if($type == 'monthly_orders')
        {
            $orders = $monthly_orders;
        }
        elseif($type == 'year_orders')
        {
            $orders = $yearly_orders;
        }
        elseif($type == 'monthly_orders_opened')
        {
            $orders = $monthly_orders->where('completed', 0)->where('canceled', 0);
        }
        elseif($type == 'year_orders_opened')
        {
            $orders = $yearly_orders->where('completed', 0)->where('canceled', 0);
        }
        elseif($type == 'monthly_orders_closed')
        {
            $orders = $monthly_orders->where('completed', 1)->where('canceled', 0);
        }
        elseif($type == 'year_orders_closed')
        {
            $orders = $yearly_orders->where('completed', 1)->where('canceled', 0);
        }
        elseif($type == 'monthly_orders_canceled')
        {
            $orders = $monthly_orders->where('canceled', 1);
        }
        elseif($type == 'year_orders_canceled')
        {
            $orders = $yearly_orders->where('canceled', 1);
        }
        elseif($type == 'monthly_parts_orders_count')
        {
            $orders = $monthly_orders->where('type','re_scheduled');
        }
        elseif($type == 'yearly_parts_orders_count')
        {
            $orders = $yearly_orders->where('type','re_scheduled');
        }

        $orders = $orders->latest()->paginate(50);

        return view('provider.collaborations.statistics_orders_dashboard',
            compact('orders', 'id', 'type','cats','company'));
    }

    public function search($id, $type,Request $request)
    {
        $company_id = Collaboration::where('id',$id)->select('company_id')->first()->company_id;
        $company = Company::where('id', $company_id)->select('id', 'en_name')->first();

        $subs = CompanySubscription::where('company_id', $company_id)->first()->subs;
        $cat_ids = Category::whereIn('id', unserialize($subs))->pluck('parent_id');
        $cats = Category::whereIn('id', $cat_ids)->select('id','en_name')->get();

        $this_year = new Carbon('first day of january this year');
        $this_month = new Carbon('first day of this month');

        $yearly_orders = Order::raw('table orders')->where('provider_id', provider()->provider_id)
            ->where('company_id', $company_id)->where('created_at','>=', $this_year->toDateTimeString());
        $monthly_orders = Order::raw('table orders')->where('provider_id', provider()->provider_id)
            ->where('company_id', $company_id)->where('created_at','>=', $this_month->toDateTimeString());

        $search = Input::get('search');

        if($type == 'monthly_orders')
        {
            $show_orders = $monthly_orders;
        }
        elseif($type == 'year_orders')
        {
            $show_orders = $yearly_orders;
        }
        elseif($type == 'monthly_orders_opened')
        {
            $show_orders = $monthly_orders->where('completed', 0)->where('canceled', 0);
        }
        elseif($type == 'year_orders_opened')
        {
            $show_orders = $yearly_orders->where('completed', 0)->where('canceled', 0);
        }
        elseif($type == 'monthly_orders_closed')
        {
            $show_orders = $monthly_orders->where('completed', 1)->where('canceled', 0);
        }
        elseif($type == 'year_ordered_closed')
        {
            $show_orders = $yearly_orders->where('completed', 1)->where('canceled', 0);
        }
        elseif($type == 'monthly_orders_canceled')
        {
            $show_orders = $monthly_orders->where('canceled', 1);
        }
        elseif($type == 'year_orders_canceled')
        {
            $show_orders = $yearly_orders->where('canceled', 1);
        }
        elseif($type == 'monthly_parts_orders_count')
        {
            $show_orders = $monthly_orders->where('type','re_scheduled');
        }
        elseif($type == 'yearly_parts_orders_count')
        {
            $show_orders = $yearly_orders->where('type','re_scheduled');
        }

        $get_orders = new Order;
        $orders = $get_orders->search($show_orders,$search,$company_id,provider()->provider_id,$request->company_id,
            $request->sub_company,$request->from,$request->to,$request->main_cats,$request->sub_cats,$request->price_range,
            $request->service_type);
        $orders = $orders['orders'];

        return view('provider.collaborations.statistics_orders_dashboard',
            compact('orders','search', 'id', 'type','company','cats'));
    }

    public function date_items($id,$type)
    {
        $company_id = Collaboration::where('id',$id)->select('company_id')->first()->company_id;

        $this_year = new Carbon('first day of january this year');
        $this_month = new Carbon('first day of this month');

        $yearly_orders = Order::raw('table orders')->where('provider_id', provider()->provider_id)
            ->where('company_id', $company_id)->where('created_at','>=', $this_year->toDateTimeString());
        $monthly_orders = Order::raw('table orders')->where('provider_id', provider()->provider_id)
            ->where('company_id', $company_id)->where('created_at','>=', $this_month->toDateTimeString());

        if($type == 'monthly_parts_count')
        {
            $orders = $monthly_orders->where('type','re_scheduled')->latest()->paginate(50);
        }
        elseif($type == 'yearly_parts_count')
        {
            $orders = $yearly_orders->where('type','re_scheduled')->latest()->paginate(50);
        }

        return view('provider.orders.show_items_dashboard',compact('orders','type'));
    }

    public function date_price($id,$type)
    {
        $company_ids = Collaboration::where('id',$id)->select('company_id')->first()->company_id;

        $this_month = new Carbon('first day of this month');
        $this_year = new Carbon('first day of january this year');

        $monthly_orders = Order::raw('table orders')->where('provider_id', provider()->provider_id)->where('company_id', $company_ids)->where('created_at','>=', $this_month->toDateTimeString());
        $yearly_orders = Order::raw('table orders')->where('provider_id', provider()->provider_id)->where('company_id', $company_ids)->where('created_at','>=', $this_year->toDateTimeString());

        if(strpos($type,'revenue'))
        {
            if($type == 'monthly_revenue')
            {
                $orders = $monthly_orders;
                $total_sum = $monthly_orders->sum('order_total');
            }
            elseif($type == 'yearly_revenue')
            {
                $orders = $yearly_orders;
                $total_sum = $yearly_orders->sum('order_total');
            }
            $orders = $orders->paginate(50);

            return view('provider.orders.price_statistics',compact('orders','company_ids','total_sum'));

        }else{
            $monthly_parts_orders = $monthly_orders->where('type','re_scheduled');
            $yearly_parts_orders = $yearly_orders->where('type','re_scheduled');

            if($type == 'monthly_parts_prices')
            {
                $monthly_parts = OrderTechRequest::whereIn('order_id', $monthly_parts_orders->pluck('id'));
                $monthly_parts_data = $monthly_parts->select('item_id','provider_id')->get();

                $monthly_arr= [];
                foreach($monthly_parts_data as $part)
                {
                    $price = DB::table($part->provider_id.'_warehouse_parts')->where('id', $part->item_id)->select('price')->first()->price;
                    array_push($monthly_arr, $price);
                }
                $total_sum = array_sum($monthly_arr);

                $orders = $monthly_parts_orders;
            }
            elseif($type == 'yearly_parts_prices')
            {
                $yearly_parts = OrderTechRequest::whereIn('order_id', $yearly_parts_orders->pluck('id'));
                $yearly_parts_data = $yearly_parts->select('item_id','provider_id')->get();

                $yearly_arr= [];
                foreach($yearly_parts_data as $part)
                {
                    $price = DB::table($part->provider_id.'_warehouse_parts')->where('id', $part->item_id)->select('price')->first()->price;
                    array_push($yearly_arr, $price);
                }
                $total_sum = array_sum($yearly_arr);

                $orders = $yearly_parts_orders;
            }
            $orders = $orders->paginate(50);

            return view('provider.orders.item_statistics',compact('orders','company_ids','total_sum'));}
    }

    public function date_rate($id,$type)
    {
        $company_ids = Collaboration::where('id',$id)->select('company_id')->first()->company_id;

        //month
        $this_month = new Carbon('first day of this month');
        $monthly_orders = Order::raw('table orders')->where('provider_id', provider()->provider_id)->where('company_id', $company_ids)->where('created_at','>=', $this_month->toDateTimeString())->get();
        $monthly_orders_ids = $monthly_orders->pluck('id');
        $monthly_rates_ids = OrderRate::whereIn('order_id', $monthly_orders_ids)->pluck('order_id');

        //year
        $this_year = new Carbon('first day of january this year');
        $yearly_orders = Order::raw('table orders')->where('provider_id', provider()->provider_id)->
        where('company_id', $company_ids)->where('created_at','>=', $this_year->toDateTimeString())->get();
        $yearly_orders_ids = $yearly_orders->pluck('id');
        $yearly_rates_ids = OrderRate::whereIn('order_id', $yearly_orders_ids)->pluck('order_id');

        if($type == 'monthly_rate'){
            $orders =  Order::whereIn('id', $monthly_rates_ids)->get();
        }elseif($type == 'yearly_rate'){
            $orders = Order::whereIn('id', $yearly_rates_ids)->get();
        }
        return view('provider.orders.rate_dashboard', compact('orders', 'type'));
    }

    public function show($collaboration_id,$id,Request $request)
    {
        $request->merge(['order_id' => $id]);
        $company_id = Collaboration::where('id',$collaboration_id)->select('company_id')->first()->company_id;
        $this->validate($request,
            [
                'order_id' => 'required|exists:orders,id,company_id,'.$company_id
            ]
        );

        $order = Order::find($id);
        $user = Order::where('id', $id)->select('user_id')->first()->user_id;
        $count = Order::where('user_id', $user)->get()->count();

        return view('provider.collaborations.show', compact('order', 'collaboration_id', 'count'));
    }

    public function bills($id)
    {
//        $collaboration = Collaboration::find($id);
//        $orders = Order::where('company_id', $collaboration->company_id)->where('completed', 1)->paginate(50);
//
//        return view('provider.bills.index', compact('orders', 'id', 'collaboration'));

        $collaboration = Collaboration::find($id);
        $company = Company::where('id', $collaboration->company_id)->select('id', 'en_name')->first();

        $subs = CompanySubscription::where('company_id', $collaboration->company_id)->first();
        if(isset($subs))
        {
            $get_subs = $subs->subs;
            $cat_ids = Category::whereIn('id', unserialize($get_subs))->pluck('parent_id');
            $cats = Category::whereIn('id', $cat_ids)->select('id','en_name')->get();

            $orders = Order::where('provider_id',provider()->provider_id)->where('company_id', $collaboration->company_id)->where('completed', 1)->latest()->paginate(50);
            return view('provider.bills.index', compact('orders', 'id', 'company', 'cats','collaboration','companies'));
        }else{

            $orders = Order::where('provider_id',provider()->provider_id)->where('company_id', $collaboration->company_id)->where('completed', 1)->latest()->paginate(50);

            return view('provider.bills.index', compact('orders', 'id', 'collaboration','company'));
        }
    }

    public function view_bills($id,Request $request)
    {
        $company_id = Collaboration::whereId($id)->select('company_id')->first()->company_id;
        $company_name = Company::whereId($company_id)->select('en_name')->first()->en_name;
        $provider_name = Provider::whereId(provider()->provider_id)->select('en_name')->first()->en_name;

        $get_orders = json_decode($request->order_data);

        $orders = [];
        $total_orders = [];
        $total_items = [];
        foreach ($get_orders as $order)
        {
            $order = Order::whereId($order->id)->first();
            array_push($orders,$order);
            array_push($total_orders,$order->order_total);
            array_push($total_items,$order->item_total);
        }
        $total_sum = array_sum($total_orders) + array_sum($total_items);
        
        return view('provider.bills.show',compact('orders','company_id','total_sum','company_name','provider_name'));
    }

    public function bills_search($id,Request $request)
    {
        $collaboration = Collaboration::find($id);
        $company = Company::where('id', $collaboration->company_id)->select('id', 'en_name')->first();
        $companies = Company::where('id', $collaboration->company_id)->select('id', 'en_name')->get();
//        $get_order = Order::where('company_id', $collaboration->company_id)->where('completed', 1);

        $subs = CompanySubscription::where('company_id', $collaboration->company_id)->first()->subs;
        $cat_ids = Category::whereIn('id', unserialize($subs))->pluck('parent_id');
        $cats = Category::whereIn('id', $cat_ids)->select('id','en_name')->get();

        $search = Input::get('search');

        $get_orders = new Order;
        $show_orders = Order::where('company_id', $collaboration->company_id)->where('completed', 1);
        $orders = $get_orders->search($show_orders,$search,$collaboration->company_id,$collaboration->provider_id,$request->company_id,
            $request->sub_company,$request->from,$request->to,$request->main_cats,$request->sub_cats,$request->price_range,
            $request->service_type);

        $bills_export = $orders['bills_export'];
        $orders = $orders['orders'];

        return view('provider.bills.index', compact('orders', 'id', 'search', 'collaboration', 'cats',
            'companies','cats','company','bills_export'));
    }

    public function get_sub_company($parent)
    {
        $sub_company = SubCompany::where('parent_id', $parent)->where('status', 'active')->select('id', 'en_name')->get();
        return response()->json($sub_company);
    }

    public function get_sub_category_provider($company_id,$parent)
    {
        $subs = CompanySubscription::where('company_id', $company_id)->first()->subs;
        $cats = Category::whereIn('id', unserialize($subs))->where('parent_id', $parent)->select('id','en_name')->get();

        return response()->json($cats);
    }

    public function bills_export($id)
    {
        $collaboration = Collaboration::find($id);

        $orders = Order::where('company_id', $collaboration->company_id)->where('completed', 1)->get();

        foreach($orders as $order)
        {
            $order['Id'] = $order->id;
            $order['Smo'] = $order->smo;
            $order['Type'] = $order->type;
            $order['User'] = $order->user->en_name;
            $order['Technician'] = $order->tech->en_name;
            $order['status'] = 'Completed';
            $order['Service Fee'] = $order->get_cat_fee($order->id);
            $order['Items Total'] = $order->item_total;
            $order['Total Amount'] = $order->order_total;
            $order['Date'] = $order->created_at;

            unset($order->tech,$order->user,$order->id,$order->smo,$order->type,$order->company_id,$order->provider_id,
                $order->cat_id,$order->tech_id,$order->user_id,$order->code,$order->completed,$order->canceled,
                $order->canceled_by,$order->item_total,$order->order_total,$order->scheduled_at,$order->created_at,
                $order->updated_at);
        }

        $orders = $orders->toArray();
        $filename = 'qareeb_bills_data.xls';


        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");

        $heads = false;
        foreach($orders as $order)
        {
            if($heads == false)
            {
                echo implode("\t", array_keys($order)) . "\n";
                $heads = true;
            }
            {
                echo implode("\t", array_values($order)) . "\n";
            }
        }

        die();

    }

    public function bills_export_search($id,$search)
    {
        $collaboration = Collaboration::find($id);
        $order = Order::where('company_id', $collaboration->company_id)->where('completed', 1);
        $orders = new Collection();

        if($search != '')
        {

            $user = User::where('company_id', $collaboration->company_id)->where(function($q) use($search)
            {
                $q->where('en_name','like','%'.$search.'%');
                $q->orWhere('ar_name','like','%'.$search.'%');
                $q->orWhere('email','like','%'.$search.'%');
                $q->orWhere('phone','like','%'.$search.'%');
                $q->orWhere('badge_id', $search);
            }
            )->first();

            $tech = Technician::where('provider_id', $collaboration->provider_id)->where(function($q) use($search)
            {
                $q->where('en_name','like','%'.$search.'%');
                $q->orWhere('ar_name','like','%'.$search.'%');
                $q->orWhere('email','like','%'.$search.'%');
                $q->orWhere('phone','like','%'.$search.'%');
                $q->orWhere('badge_id', $search);
            }
            )->first();

            if($search == isset(Order::where('company_id', $collaboration->company_id)->where('id', $search)->first()->id))
            {
                $by_order_id = $order->where('id', $search)->get();
                $orders = $orders->merge($by_order_id);
            }
            if($search == isset(Order::where('smo', $search)->first()->smo))
            {
                $by_smo = $order->where('smo', $search)->get();
                $orders = $orders->merge($by_smo);
            }
            if($user)
            {
                $by_user =$order->where('user_id', $user->id)->get();
                $orders = $orders->merge($by_user);
            }
            if($tech)
            {
                $by_tech = $order->where('tech_id', $tech->id)->get();
                $orders = $orders->merge($by_tech);
            }

        }else{
            $all = $order->get();
            $orders = $orders->merge($all);
        }

        foreach($orders as $order)
        {
            $order['Id'] = $order->id;
            $order['Smo'] = $order->smo;
            $order['Type'] = $order->type;
            $order['User'] = $order->user->en_name;
            $order['Technician'] = $order->tech->en_name;
            $order['status'] = 'Completed';
            $order['Service Fee'] = $order->get_cat_fee($order->id);
            $order['Items Total'] = $order->item_total;
            $order['Total Amount'] = $order->order_total;
            $order['Date'] = $order->created_at;

            unset($order->tech,$order->user,$order->id,$order->smo,$order->type,$order->company_id,$order->provider_id,
                $order->cat_id,$order->tech_id,$order->user_id,$order->code,$order->completed,$order->canceled,
                $order->canceled_by,$order->item_total,$order->order_total,$order->scheduled_at,$order->created_at,
                $order->updated_at);
        }

        $orders = $orders->toArray();
        $filename = 'qareeb_bills_data.xls';


        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");

        $heads = false;
        foreach($orders as $order)
        {
            if($heads == false)
            {
                echo implode("\t", array_keys($order)) . "\n";
                $heads = true;
            }
            {
                echo implode("\t", array_values($order)) . "\n";
            }
        }

    }

//    public function print_bills()
//    {
//        return view('provider.bill')
//    }
}
