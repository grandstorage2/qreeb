<?php

namespace App\Http\Controllers\Provider;

use App\Models\Category;
use App\Models\Collaboration;
use App\Models\Provider;
use App\Models\ProviderCategoryFee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceFeeController extends Controller
{
    public function view($id)
    {
        $collaboration = Collaboration::find($id)->company_id;

        $ids = ProviderCategoryFee::where('provider_id', provider()->provider_id)
            ->where('company_id', $collaboration)->pluck('cat_id');
        $parents = Category::whereIn('id', $ids)->where('type', 2)->pluck('parent_id');

        $cats = Category::whereIn('id', $parents)->get();

        return view('provider.services.single', compact('cats','collaboration'));
    }


    public function update(Request $request)
    {
        foreach($request->urgent_fees as $inc => $data)
        {
            $this_request = new Request(array_keys($data));
            $this->validate($this_request,
                [
                    '0' => 'required|exists:categories,id,type,2'
                ]
            );

            foreach($data as $id => $fee)
            {
                ProviderCategoryFee::updateOrcreate
                (
                    [
                        'provider_id' => provider()->provider_id,
                        'company_id' => $request->company_id,
                        'cat_id' => $id
                    ],
                    [
                        'urgent_fee' => $fee
                    ]
                );
            }
        }

        foreach($request->scheduled_fees as $inc => $data)
        {
            $this_request = new Request(array_keys($data));
            $this->validate($this_request,
                [
                    '0' => 'required|exists:categories,id,type,2'
                ]
            );

            foreach($data as $id => $fee)
            {
                ProviderCategoryFee::updateOrcreate
                (
                    [
                        'provider_id' => provider()->provider_id,
                        'company_id' => $request->company_id,
                        'cat_id' => $id
                    ],
                    [
                        'scheduled_fee' => $fee
                    ]
                );
            }
        }

        return back()->with('success', 'Services Fees updated successfully');
    }
}
