<?php

namespace App\Http\Controllers\Company;

use App\Models\Category;
use App\Models\Collaboration;
use App\Models\Company;
use App\Models\CompanySubscription;
use App\Models\Order;
use App\Models\OrderTechDetail;
use App\Models\OrderUserDetail;
use App\Models\ProviderCategoryFee;
use App\Models\SubCompany;
use App\Models\Technician;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\OrdersImport;

class OrderController extends Controller
{
    public function index($type)
    {

        $collaboration = Collaboration::where('company_id',company()->company_id)->pluck('provider_id');
        $companies = Company::whereIn('id', $collaboration)->select('id', 'en_name')->get();

        $subs = CompanySubscription::where('company_id', company()->company_id)->first()->subs;
        $cat_ids = Category::whereIn('id', unserialize($subs))->pluck('parent_id');
        $cats = Category::whereIn('id', $cat_ids)->select('id','en_name')->get();

        $provider_ids = Collaboration::where('company_id',company()->company_id)->pluck('provider_id');
        $this_month = new Carbon('first day of this month');
        $monthly_orders = Order::raw('table orders')->whereIn('provider_id', $provider_ids)->where('company_id', company()->company_id)->where('created_at','>=', $this_month->toDateTimeString());

        $this_year = new Carbon('first day of january this year');
        $yearly_orders = Order::raw('table orders')->whereIn('provider_id', $provider_ids)->where('company_id', company()->company_id)->where('created_at','>=', $this_year->toDateTimeString())->get();

        if($type == 'monthly_parts_orders_count')
        {
            $orders = $monthly_orders->where('type','re_scheduled');
        }elseif($type == 'yearly_parts_orders_count')
        {
            $orders = $yearly_orders->where('type','re_scheduled');
        }else{
            $orders = Order::where('company_id', company()->company_id)->where('type', $type);
        }

        $orders = $orders->latest()->paginate(50);

        return view('company.orders.index', compact('orders','type','companies','cats'));
    }

    public function search($state,Request $request)
    {
        $search = Input::get('search');

        $subs = CompanySubscription::where('company_id', company()->company_id)->first()->subs;
        $cat_ids = Category::whereIn('id', unserialize($subs))->pluck('parent_id');
        $cats = Category::whereIn('id', $cat_ids)->select('id','en_name')->get();

        $collaboration = Collaboration::where('company_id',company()->company_id)->pluck('provider_id');
        $companies = Company::whereIn('id', $collaboration)->select('id', 'en_name')->get();

        if($search != '')
        {
            $user = User::where('company_id', company()->company_id)->where(function($q) use($search)
            {
                $q->where('en_name','like','%'.$search.'%');
                $q->orWhere('ar_name','like','%'.$search.'%');
            }
            )->first();

            $tech = Technician::whereIn('provider_id', $collaboration)->where(function($q) use($search)
            {
                $q->where('en_name','like','%'.$search.'%');
                $q->orWhere('ar_name','like','%'.$search.'%');
            }
            )->first();

            $orders = Order::where('company_id', company()->company_id)->where('type',$state);

            if(is_numeric($search))
            {
                $orders = Order::where('company_id', company()->company_id)->where('type',$state)->where(function($q) use($search)
                {
                    $q->where('id','like','%'.$search.'%');
                    $q->orWhere('smo','like','%'.$search.'%');
                }
                );
            }else
            {
                if($user)
                {
                    $orders = $orders->where('user_id', $user->id);
                }
                if($tech)
                {
                    $orders = $orders->where('tech_id', $tech->id);
                }
            }

            if($request->has('company_id'))
            {
                $orders = $orders->where('company_id', $request->company_id);
            }

            if($request->has('sub_company'))
            {
                $get_sub = User::where('company_id', company()->company_id)->where('sub_company_id', $request->sub_company)->pluck('id');
                $orders = $orders->whereIn('user_id', $get_sub);            }

            if($request->from && $request->to)
            {
                $orders = $orders->where('created_at','>=',$request->from)->where('created_at','<=',Carbon::parse($request->to)->addDays(1));
            }

            if($request->has('main_cats'))
            {
                $cat = Category::where('id', $request->main_cats)->first();
                $sub_cat = Category::where('parent_id', $cat->id)->pluck('id');
                $orders = $orders->whereIn('sub_cat_id', $sub_cat);
            }

            if($request->has('sub_cats'))
            {
                $orders = $orders->where('sub_cat_id', $request->sub_cats);
            }

            $orders = $orders->paginate(50);

            $from = $request->from;
            $to = $request->to;
            $company = Company::where('id',$request->company_id)->first();
            $sub_company = SubCompany::where('id',$request->sub_company)->first();
            $main_cats = Category::where('id',$request->main_cats)->first();
            $sub_cats = Category::where('id',$request->sub_cats)->first();
        }else{

            $orders = Order::where('company_id', company()->company_id)->where('type',$state);

            if($request->has('company_id'))
            {
                $orders = $orders->where('company_id', $request->company_id);
            }

            if($request->has('sub_company'))
            {
                $get_sub = User::where('company_id', company()->company_id)->where('sub_company_id', $request->sub_company)->pluck('id');
                $orders = $orders->whereIn('user_id', $get_sub);
            }

            if($request->from && $request->to)
            {
                $orders = $orders->where('created_at','>=',$request->from)->where('created_at','<=',Carbon::parse($request->to)->addDays(1));
            }

            if($request->has('main_cats'))
            {
                $cat = Category::where('id', $request->main_cats)->first();
                $sub_cat = Category::where('parent_id', $cat->id)->pluck('id');
                $orders = $orders->whereIn('sub_cat_id', $sub_cat);
            }

            if($request->has('sub_cats'))
            {
                $orders = $orders->where('sub_cat_id', $request->sub_cats);
            }

            $orders = $orders->paginate(50);

            $from = $request->from;
            $to = $request->to;
            $sub_company = SubCompany::where('id',$request->sub_company)->first();
            $main_cats = Category::where('id',$request->main_cats)->first();
            $sub_cats = Category::where('id',$request->sub_cats)->first();
            $company = Company::where('id',$request->company_id)->first();
        }


        return view('company.orders.search', compact('orders','search',
            'from', 'to', 'sub_company', 'main_cats', 'sub_cats','companies','company','cats'));
    }


    public function show($id,Request $request)
    {
        $request->merge(['order_id' => $id]);
        $this->validate($request,
            [
                'order_id' => 'required|exists:orders,id,company_id,'.company()->company_id
            ]
        );

        $order = Order::find($id);
        $user = Order::where('id', $id)->select('user_id')->first()->user_id;
        $count = Order::where('user_id', $user)->get()->count();

        $tech_details = OrderTechDetail::where('order_id', $id)->get();

        return view('company.orders.show', compact('order', 'count','tech_details'));
    }


    public function orders_request($type, Request $request)
    {
        $request->merge(['type' => $type]);
        $this->validate($request,
            [
                'type' => 'required|in:urgent,scheduled,re_scheduled,canceled'
            ]
        );

        $types['urgent'] = 'Urgent';
        $types['scheduled'] = 'Scheduled';
        $types['re_scheduled'] = 'Re-scheduled';
        $types['canceled'] = 'Canceled';

        return view('company.orders.orders_request', compact('type','types'));
    }


    public function orders_show(Request $request)
    {
        $this->validate($request,
            [
                'type' => 'in:urgent,scheduled,re_scheduled,canceled',
                'from' => 'required|date',
                'to' => 'required|date'
            ],
            [
                'type.required' => 'Please choose a type',
                'type.exists' => 'Invalid Type',
                'from.required' => 'Please choose a date to start from',
                'from.date' => 'Please choose a valid date to start from',
                'to.required' => 'Please choose a date to end with',
                'to.date' => 'Please choose a valid date to end with',
            ]
        );

        if($request->type == 'canceled')
        {
            $orders = Order::where('company_id', company()->company_id)->where('canceled', 1)->where('created_at', '>=', $request->from)->where('created_at', '<=', $request->to)->get();
        }
        else
        {
            $orders = Order::where('company_id', company()->company_id)->where('type', $request->type)->where('created_at', '>=', $request->from)->where('created_at', '<=', $request->to)->get();
        }

        $orders[] = collect(['total' => $orders->sum('order_total')]);

        if($request->type == 'urgent') $type_key = 'urgent' ; $type_value = 'Urgent';
        if($request->type == 'scheduled') $type_key = 'scheduled' ; $type_value = 'Scheduled';
        if($request->type == 're_scheduled') $type_key = 're_scheduled' ; $type_value = 'Re-Scheduled';
        if($request->type == 'canceled') $type_key = 'canceled' ; $type_value = 'Canceled';

        $from = $request->from;
        $to = $request->to;

        return view('company.orders.orders_show', compact('orders','type_key','type_value','from','to'));
    }


    public function orders_export(Request $request)
    {
        $this->validate($request,
            [
                'type' =>'in:urgent,scheduled,re_scheduled,canceled',
                'from' => 'required|date',
                'to' => 'required|date'
            ]
        );


        $orders = new Collection();
        if($request->type == 'canceled')
        {
            $get_orders = Order::where('company_id', company()->company_id)->where('canceled', 1)->where('created_at', '>=', $request->from)->where('created_at', '<=', $request->to)->get();
        }
        else
        {
            $get_orders = Order::where('company_id', company()->company_id)->where('type', $request->type)->where('created_at','>=',$request->from)->where('created_at','<=',$request->to)->get();
        }

        foreach($get_orders as $order)
        {
            if($order->type == 'urgent') $type = 'Urgent';
            elseif($order->type == 'scheduled') $type = 'Scheduled';
            elseif($order->type == 're_scheduled') $type = 'Re-Scheduled';


            $collect['Category'] = $order->category->parent->en_name . ' - ' . $order->category->en_name;
            $collect['Date'] = $order->created_at->toDateTimeString();
            $collect['Type'] = $type;
            if($order->canceled == 1)
            {
                if($order->canceled_by== 'user') $by = 'User';
                else $by = 'Tech';

                $collect['Canceled By'] = $by;
            }
            else
            {
                $collect['Canceled By'] = '-';
            }

            $collect['Cost'] = $order->order_total;
            $collect['Total'] = '';

            $orders = $orders->push($collect);
        }

        if($request->type == 'canceled')
        {
            $orders[] = collect(['Category' => '-','Date' => '-','Type' => '-','By' => '-','Cost' => '-','Total' => $orders->sum('Cost')]);
        }
        else
        {
            $orders[] = collect(['Category' => '-', 'Date' => '-', 'Type' => '-', 'Cost' => '-', 'Total' => $orders->sum('Cost')]);
        }

        if($get_orders->count() > 0)
        {
            $orders = $orders->toArray();

            $company = Company::where('id', company()->company_id)->select('en_name')->first();
            $from = $request->from;
            $to = $request->to;
            $p_name = str_replace(' ','-',$company->en_name);

            $filename = 'qareeb_'.$p_name.'_'.$type.'_'.$from.'_'.$to.'_orders_invoice.xls';

            header("Content-Disposition: attachment; filename=\"$filename\"");
            header("Content-Type: application/vnd.ms-excel");

            $heads = false;
            foreach($orders as $order)
            {
                if($heads == false)
                {
                    echo implode("\t", array_keys($order)) . "\n";
                    $heads = true;
                }
                {
                    echo implode("\t", array_values($order)) . "\n";
                }
            }

            die();
        }
        else
        {
            return redirect('/company/orders/'.$request->type)->with('error', 'No Result !');
        }

    }

    public function excel_view($type)
    {
        return view('company.orders.upload', compact('type'));
    }

    public function excel_upload(Request $request)
    {
        $this->validate($request,
            [
                'file' => 'required|file'
            ]
        );
        $array = Excel::toArray(new OrdersImport(),$request->file('file'));
//        unset($array[0][0]);

        if($request->type === 'urgent'){

            foreach($array[0] as $data){
                $data = array_filter($data);
                try{
                    $request->merge(['id' => $data[0], 'smo' => $data[1], 'provider_id' => $data[2],
                        'service_type'=>$data[3], 'cat_id' => $data[4],'tech_id' => $data[5], 'user_id' => $data[6],
                        'created_at'=>$data[10] ]);
                }

                catch (\Exception $e)
                {
                    return back()->with('error','Missing Column | '.$e->getMessage().',Offsets start from 0');
                }

                $this->validate($request,
                    [
                        'id' => 'unique:orders,id',
                        'smo' => 'required',
                        'provider_id' => 'required|exists:providers,id',
                        'service_type' => 'required|in:1,2,3',
                        'cat_id' => 'required|exists:categories,id,type,2',
                        'tech_id' => 'required|exists:technicians,badge_id',
                        'user_id' => 'required|exists:users,badge_id',
                    ],
                    [
                        'smo.required' => 'Missing data in SMO column.',
                        'service_type.required' => 'Missing data in Service Type column.',
                        'service_type.in' => 'Service type must be 1 or 2 or 3.',
                        'provider_id.required' => 'Missing data in Provider ID column.',
                        'provider_id.exists' => 'Wrong ID in Provider ID column '.$request->provider_id.'.',
                        'cat_id.required' => 'Missing data in Category ID column.',
                        'cat_id.exists' => 'Wrong ID in Category ID column '.$request->cat_id.'.',
                        'tech_id.required' => 'Missing data in Technician ID column.',
                        'tech_id.exists' => 'Wrong ID in Technician ID column '.$request->tech_id.'.',
                        'user_id.required' => 'Missing data in User ID column.',
                        'user_id.exists' => 'Wrong ID in User ID column '.$request->user_id.'.',
                    ]
                );

                // Numbers of days between January 1, 1900 and 1970 (including 19 leap years)
//                define("MIN_DATES_DIFF", 25569);
//
//                // Numbers of second in a day:
//                define("SEC_IN_DAY", 86400 - 3);
//                function excel2timestamp($excelDate)
//                {
//                    if ($excelDate <= MIN_DATES_DIFF)
//                        return 0;
//
//                    return  ($excelDate - MIN_DATES_DIFF) * SEC_IN_DAY;
//                }
//
//                $created_at = excel2timestamp($data[10]);

                $UNIX_DATE = ($data[10] - 25569) * 86400;
                $created_at = gmdate("Y-m-d H:i:s", $UNIX_DATE);

                $provider_fee = ProviderCategoryFee::where('provider_id', $data[2])->where('cat_id', $data[4])->first()->urgent_fee;

                $tech = Technician::where('badge_id', $data[5])->select('id')->first();
                $user = User::where('badge_id', $data[6])->select('id')->first();

                $order = new Order();
                $order->timestamps = false;

                $order->id = $data[0];
                $order->smo = $data[1];
                $order->type = 'urgent';
                $order->company_id = company()->company_id;
                $order->provider_id = $data[2];
                $order->service_type = $data[3];
                $order->cat_id = $data[4];
                $order->sub_cat_id = $data[4];
                $order->tech_id = $tech->id;
                $order->user_id = $user->id;
                $order->completed = 1;
                $order->code = rand(1000, 9999);
                $order->created_at = $created_at;
                if($provider_fee)
                {
                    $order->order_total =  $provider_fee;
                }else{
                    $order->order_total = Category::where('id', $data[4])->first()->urgent_price;
                }
                $order->save();

                if(isset($data[7])  && isset($data[8]) && isset($data[9]))
                {
                    $order_details = new OrderUserDetail();
                    $order_details->order_id = $order->id;
                    $order_details->place = $data[7];
                    $order_details->part = $data[8];
                    $order_details->desc = $data[9];
                    $order_details->created_at = $created_at;
                    $order_details->save();
                }
            }

        }
        else{

            foreach($array[0] as $data){
                $data = array_filter($data);

                try{
                    $request->merge(['id' => $data[0], 'smo' => $data[1], 'provider_id' => $data[2],
                        'service_type'=>$data[3],'cat_id' => $data[4],'tech_id' => $data[5], 'user_id' => $data[6],
                        'scheduled_at' => $data[10],'created_at' => $data[11] ]);

                }

                catch (\Exception $e)
                {
                    return back()->with('error','Missing Column | '.$e->getMessage().',Offsets start from 0');
                }


                $this->validate($request,
                    [
                        'id' => 'unique:orders,id',
                        'smo' => 'required|unique:orders,smo',
                        'provider_id' => 'required|exists:providers,id',
                        'service_type' => 'required',
                        'cat_id' => 'required|exists:categories,id,type,2',
                        'tech_id' => 'required|exists:technicians,badge_id',
                        'user_id' => 'required|exists:users,badge_id',
                        'scheduled_at' => 'required'
                    ],
                    [
                        'smo.required' => 'Missing data in SMO column.',
                        'provider_id.required' => 'Missing data in Provider ID column.',
                        'service_type.required' => 'Missing data in Service Type column.',
                        'provider_id.exists' => 'Wrong ID in Provider ID column '.$request->provider_id.'.',
                        'cat_id.required' => 'Missing data in Category ID column.',
                        'cat_id.exists' => 'Wrong ID in Category ID column '.$request->cat_id.'.',
                        'tech_id.required' => 'Missing data in Technician ID column.',
                        'tech_id.exists' => 'Wrong ID in Technician ID column '.$request->tech_id.'.',
                        'user_id.required' => 'Missing data in User ID column.',
                        'user_id.exists' => 'Wrong ID in User ID column '.$request->user_id.'.',
                        'scheduled_at.required' => 'Missing data in Scheduled At Total column.',
                    ]
                );

// Numbers of days between January 1, 1900 and 1970 (including 19 leap years)
//                define("MIN_DATES_DIFF", 25569);
//
//// Numbers of second in a day:
//                define("SEC_IN_DAY", 86400 - 3);
//                function excel2timestamp($excelDate)
//                {
//                    if ($excelDate <= MIN_DATES_DIFF)
//                        return 0;
//
//                    return  ($excelDate - MIN_DATES_DIFF) * SEC_IN_DAY;
//                }
//
//                $time = excel2timestamp($data[10]);
//                $created_at = excel2timestamp($data[11]);

                $UNIX_DATE = ($data[10] - 25569) * 86400;
                $scheduled_at = gmdate("Y-m-d H:i:s", $UNIX_DATE);

                $UNIX_DATE = ($data[11] - 25569) * 86400;
                $created_at = gmdate("Y-m-d H:i:s", $UNIX_DATE);

                $provider_fee = ProviderCategoryFee::where('provider_id', $data[2])->where('cat_id', $data[4])->first()->scheduled_fee;

                $tech = Technician::where('badge_id', $data[5])->select('id')->first();
                $user = User::where('badge_id', $data[6])->select('id')->first();

                $order = new Order();
                $order->timestamps = false;

                $order->id = $data[0];
                $order->smo = $data[1];
                $order->type = 'scheduled';
                $order->company_id = company()->company_id;
                $order->provider_id = $data[2];
                $order->service_type = $data[3];
                $order->cat_id = $data[4];
                $order->sub_cat_id = $data[4];
                $order->tech_id = $tech->id;
                $order->user_id = $user->id;
                $order->completed = 1;
                $order->code = rand(1000, 9999);
                $order->scheduled_at = $scheduled_at;
                $order->created_at = $created_at;
                if($provider_fee)
                {
                    $order->order_total =  $provider_fee;
                }else{
                    $order->order_total = Category::where('id', $data[4])->first()->scheduled_price;
                }
                $order->save();

                if(isset($data[7])  && isset($data[8]) && isset($data[9]))
                {
                    $order_details = new OrderUserDetail();
                    $order_details->order_id = $order->id;
                    $order_details->place = $data[7];
                    $order_details->part = $data[8];
                    $order_details->desc = $data[9];
                    $order_details->created_at = $created_at;
                    $order_details->save();
                }

//                $order = Order::create
////                (
////                    [
////                        'smo' => $data[0],
////                        'type' => 'scheduled',
////                        'company_id' => company()->company_id,
////                        'provider_id' => $data[1],
////                        'cat_id' => $data[2],
////                        'tech_id' => $data[3],
////                        'user_id' => $data[4],
////                        'completed' => 1,
////                        'code' => rand(1000, 9999),
////                        'item_total' => $data[8],
////                        'order_total' => $data[9],
////                        'scheduled_at' => date('Y-m-d H:i', $time),
////                        'created_at' => date('Y-m-d H:i', $created_at),
//////
////                    ]
////                );
            }

        }

        return redirect('/company/orders/'.$request->type.'/excel/view')->with('success', 'Order uploaded successfully');
    }

    public function excel_open_view($type)
    {
        return view('company.orders.upload_open', compact('type'));
    }

    public function excel_open_upload(Request $request)
    {
        $this->validate($request,
            [
                'file' => 'required|file'
            ]
        );

        $collaborations = Collaboration::where('company_id', company()->company_id)->select('provider_id')->get();
        $file = $request->file->getClientOriginalName();
        foreach ($collaborations as $collaboration)
        {
            if($file == $collaboration->provider_id.'_'.\company()->company_id.'_'.Carbon::now()->format('Y-m-d').'.xlsx')
            {
                $request->file->move(base_path() .'/public/orders/waiting/',$file);

                return redirect('/company/orders/open/'.$request->type.'/excel/view')->with('success', 'Order uploaded successfully');
            }else{
                return redirect('/company/orders/open/'.$request->type.'/excel/view')->with('error', 'File name must be providerID_companyID_dateNow');
            }

        }
    }

    public function bills()
    {
        $orders = Order::where('company_id', company()->company_id)->where('completed', 1)->paginate(50);

        return view('company.bills.index', compact('orders'));
    }

    public function bills_export()
    {

        $orders = Order::where('company_id', company()->company_id)->where('completed', 1)->get();

        foreach($orders as $order)
        {
            $order['Id'] = $order->id;
            $order['Smo'] = $order->smo;
            $order['Type'] = $order->type;
            $order['Technician'] = $order->tech->en_name;
            $order['status'] = 'Completed';
            $order['Service Fee'] = $order->get_cat_fee($order->id);
            $order['Items Total'] = $order->item_total;
            $order['Total Amount'] = $order->order_total;

            unset($order->tech,$order->id,$order->smo,$order->type,$order->company_id,$order->provider_id,
            $order->cat_id,$order->tech_id,$order->user_id,$order->code,$order->completed,$order->canceled,
            $order->canceled_by,$order->item_total,$order->order_total,$order->scheduled_at,$order->created_at,
            $order->updated_at);
        }

        $orders = $orders->toArray();
        $filename = 'qareeb_bills_data.xls';


        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");

        $heads = false;
        foreach($orders as $order)
        {
            if($heads == false)
            {
                echo implode("\t", array_keys($order)) . "\n";
                $heads = true;
            }
            {
                echo implode("\t", array_values($order)) . "\n";
            }
        }

        die();

    }
}
