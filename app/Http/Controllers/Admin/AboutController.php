<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\AboutUs;
use App\Models\Complain;
use App\Models\Privacy;
use App\Models\Term;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        $about = AboutUs::first();

        return view('admin.settings.abouts.index', compact('about'));
    }


    public function edit()
    {
        $about = AboutUs::first();
        return view('admin.settings.abouts.single', compact('about'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'en_text' => 'required',
                'ar_text' => 'required',
            ],
            [
                'en_text.required' => 'English text is required',
                'ar_text.required' => 'Arabic text is required',
            ]
        );

        $about = AboutUs::first();
            $about->en_text = $request->en_text;
            $about->ar_text = $request->ar_text;
        $about->save();

        return redirect('/admin/settings/about')->with('success', 'Updated successfully');
    }

    public function terms()
    {
        $term = Term::first();

        return view('admin.settings.terms.index', compact('term'));
    }


    public function terms_edit()
    {
        $term = Term::first();
        return view('admin.settings.terms.single', compact('term'));
    }


    public function terms_update(Request $request)
    {
        $this->validate($request,
            [
                'en_text' => 'required',
                'ar_text' => 'required',
            ],
            [
                'en_text.required' => 'English text is required',
                'ar_text.required' => 'Arabic text is required',
            ]
        );

        $about = Term::first();
        $about->en_text = $request->en_text;
        $about->ar_text = $request->ar_text;
        $about->save();

        return redirect('/admin/settings/terms')->with('success', 'Updated successfully');
    }

    public function privacy()
    {
        $privacy = Privacy::first();

        return view('admin.settings.privacy.index', compact('privacy'));
    }


    public function privacy_edit()
    {
        $privacy = Privacy::first();
        return view('admin.settings.privacy.single', compact('privacy'));
    }


    public function privacy_update(Request $request)
    {
        $this->validate($request,
            [
                'en_text' => 'required',
                'ar_text' => 'required',
            ],
            [
                'en_text.required' => 'English text is required',
                'ar_text.required' => 'Arabic text is required',
            ]
        );

        $privacy = Privacy::first();
        $privacy->en_text = $request->en_text;
        $privacy->ar_text = $request->ar_text;
        $privacy->save();

        return redirect('/admin/settings/privacy')->with('success', 'Updated successfully');
    }

    public function complains()
    {
        $complains = Complain::get();

        return view('admin.settings.complains.index', compact('complains'));
    }


}
