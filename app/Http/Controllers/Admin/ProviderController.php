<?php

namespace App\Http\Controllers\Admin;

use App\Migrations\WareHouse;
use App\Migrations\WareHouseRequest;
use App\Models\Address;
use App\Models\Category;
use App\Models\Collaboration;
use App\Models\Company;
use App\Models\Order;
use App\Models\OrderRate;
use App\Models\OrderTechRequest;
use App\Models\Provider;
use App\Models\ProviderAdmin;
use App\Models\ProviderCategoryFee;
use App\Models\ProviderSubscription;
use App\Models\SubCompany;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use mysql_xdevapi\Exception;

class ProviderController extends Controller
{
    public function index($state)
    {
        if($state == 'active')
        {
            $providers = Provider::where('active' , 1)->paginate(50);
        }
        elseif($state == 'suspended')
        {
            $providers = Provider::where('active' , 0)->paginate(50);
        }

        return view('admin.providers.index', compact('providers'));
    }

    public function search()
    {
        $search = Input::get('search');
        $providers = Provider::where(function ($q) use ($search)
            {
                $q->where('en_name','like','%'.$search.'%');
                $q->orWhere('ar_name','like','%'.$search.'%');
                $q->orWhere('email','like','%'.$search.'%');
            }
        )->paginate(50);

        return view('admin.providers.index', compact('providers','search'));
    }


    public function show($id)
    {
        $provider = Provider::find($id);
        return view('admin.providers.show', compact('provider'));
    }


    public function statistics($id)
    {
        $provider = Provider::find($id);
        $company_ids = Collaboration::where('provider_id',$provider->id)->pluck('company_id');


        $this_month = new Carbon('first day of this month');
        $this_year = new Carbon('first day of january this year');

        $monthly_orders = Order::raw('table orders select * from orders')->where('provider_id', $provider->id)->whereIn('company_id', $company_ids)->where('created_at','>=', $this_month->toDateTimeString())->get();
        $monthly_orders_ids = $monthly_orders->pluck('id');

        $monthly_orders_count = $monthly_orders->count();
        $monthly_open = $monthly_orders->where('completed', 0)->count();
        $monthly_closed = $monthly_orders->where('completed', 1)->count();
        $monthly_canceled = $monthly_orders->where('canceled', 1)->count();
        $monthly_canceled_user = $monthly_orders->where('canceled', 1)->where('canceled_by', 'user')->count();
        $monthly_canceled_tech = $monthly_orders->where('canceled', 1)->where('canceled_by', 'tech')->count();
        $monthly_revenue = $monthly_orders->sum('order_total');

        $monthly_parts_orders = $monthly_orders->where('type','re_scheduled');
        $monthly_parts_orders_count = $monthly_parts_orders->count();
        $monthly_parts = OrderTechRequest::whereIn('order_id', $monthly_parts_orders->pluck('id'));
        $monthly_parts_count = $monthly_parts->count();
        $monthly_parts_data = $monthly_parts->select('item_id','provider_id')->get();

        $monthly_arr= [];
        foreach($monthly_parts_data as $part)
        {
            $price = DB::table($part->provider_id.'_warehouse_parts')->where('id', $part->item_id)->select('price')->first()->price;
            array_push($monthly_arr, $price);
        }
        $monthly_parts_prices = array_sum($monthly_arr);

        $monthly_rates_ids = OrderRate::whereIn('order_id', $monthly_orders_ids)->get();
        $monthly_rate_commitment = (string)round($monthly_rates_ids->pluck('commitment')->avg(),0);
        $monthly_rate_cleanliness = (string)round($monthly_rates_ids->pluck('cleanliness')->avg(),0);
        $monthly_rate_performance = (string)round($monthly_rates_ids->pluck('performance')->avg(),0);
        $monthly_rate_appearance = (string)round($monthly_rates_ids->pluck('appearance')->avg(),0);

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $yearly_orders = Order::raw('table orders select * from orders')->where('provider_id', $provider->id)->whereIn('company_id', $company_ids)->where('created_at','>=', $this_year->toDateTimeString())->get();
        $yearly_orders_ids = $yearly_orders->pluck('id');

        $yearly_orders_count = $yearly_orders->count();
        $yearly_open = $yearly_orders->where('completed', 0)->count();
        $yearly_closed = $yearly_orders->where('completed', 1)->count();
        $yearly_canceled = $yearly_orders->where('canceled', 1)->count();
        $yearly_canceled_user = $yearly_orders->where('canceled', 1)->where('canceled_by', 'user')->count();
        $yearly_canceled_tech = $yearly_orders->where('canceled', 1)->where('canceled_by', 'tech')->count();
        $yearly_revenue = $yearly_orders->sum('order_total');

        $yearly_parts_orders = $yearly_orders->where('type','re_scheduled');
        $yearly_parts_orders_count = $yearly_parts_orders->count();
        $yearly_parts = OrderTechRequest::whereIn('order_id', $yearly_parts_orders->pluck('id'));
        $yearly_parts_count = $yearly_parts->count();
        $yearly_parts_data = $yearly_parts->select('item_id','provider_id')->get();

        $yearly_arr= [];
        foreach($yearly_parts_data as $part)
        {
            $price = DB::table($part->provider_id.'_warehouse_parts')->where('id', $part->item_id)->select('price')->first()->price;
            array_push($yearly_arr, $price);
        }
        $yearly_parts_prices = array_sum($yearly_arr);

        $yearly_rates_ids = OrderRate::whereIn('order_id', $yearly_orders_ids)->get();
        $yearly_rate_commitment = (string)round($yearly_rates_ids->pluck('commitment')->avg(),0);
        $yearly_rate_cleanliness = (string)round($yearly_rates_ids->pluck('cleanliness')->avg(),0);
        $yearly_rate_performance = (string)round($yearly_rates_ids->pluck('performance')->avg(),0);
        $yearly_rate_appearance = (string)round($yearly_rates_ids->pluck('appearance')->avg(),0);

        return view('admin.providers.statistics',
            compact('provider','this_month','this_year','monthly_orders_count','monthly_open','monthly_closed','monthly_canceled','monthly_canceled_user','monthly_canceled_tech'
                ,'monthly_revenue','monthly_parts_orders','monthly_parts_orders_count','monthly_parts_count','monthly_parts_prices','monthly_rate_commitment','monthly_rate_appearance','monthly_rate_cleanliness'
                ,'monthly_rate_performance','yearly_orders_count','yearly_open','yearly_closed','yearly_canceled','yearly_canceled_user','yearly_canceled_tech','yearly_revenue','yearly_parts_orders','yearly_parts_orders_count'
                ,'yearly_parts_count','yearly_parts_prices','yearly_rate_commitment','yearly_rate_cleanliness','yearly_rate_performance','yearly_rate_appearance'
            )
        );
    }


    public function create()
    {
        $addresses = Address::where('parent_id', NULL)->get();
        return view('admin.providers.single', compact('addresses'));
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'address_id' => 'required',
                'interest_fee' => 'required|integer',
                'warehouse_fee' => 'required|integer',
                'ar_name' => 'required|unique:providers,ar_name',
                'en_name' => 'required|unique:providers,en_name',
                'ar_desc' => 'required',
                'en_desc' => 'required',
                'email' => 'required|email|unique:providers,email',
                'phones' => 'required|array',
                'logo' => 'required|image',
                'username' => 'required|unique:provider_admins,username',
                'password' => 'required|min:6|confirmed',
                'mobile' => 'required|unique:provider_admins,phone',
                'badge_id' => 'required'
            ],
            [
                'address_id.required' => 'Address is required',
                'interest_fee.required' => 'Interest Fee is required',
                'warehouse_fee.required' => 'Warehouse Fee is required',
                'ar_name.required' => 'Arabic name is required',
                'ar_name.unique' => 'Arabic name already exists',
                'en_name.required' => 'English name is required',
                'en_name.unique' => 'English name already exists',
                'ar_desc.required' => 'Arabic description is required',
                'en_desc.required' => 'English description is required',
                'email.required' => 'Email is required',
                'email.unique' => 'Email already exists',
                'phones.required' => 'Phones are required',
                'logo.required' => 'Logo is required',
                'logo.image' => 'Logo is not valid',
                'username.required' => 'Username is required',
                'username.unique' => 'Username already exists',
                'password.required' => 'Password is required',
                'password.min' => 'Password must be 6 digits at minimum',
                'password.confirmed' => 'Password and its confirmation does not match',
                'mobile.required' => 'Admin Mobile is required',
                'mobile.unique' => 'Admin Mobile already exists',
                'badge_id.required' => 'Admin Badge ID is required',
            ]
        );


        $image = unique_file($request->logo->getClientOriginalName());
        $image1 = unique_file($request->logo->getClientOriginalName());

        $request->logo->move(base_path().'/public/providers/logos/', $image);

        if(!File::exists(base_path().'/public/providers/admins'))
        {
            File::makeDirectory(base_path().'/public/providers/admins');
        }

        File::copy(base_path().'/public/providers/logos/'.$image,base_path().'/public/providers/admins/'.$image1);

        $provider = Provider::create(
            [
                'address_id' => $request->address_id,
                'interest_fee' => $request->interest_fee,
                'warehouse_fee' => $request->warehouse_fee,
                'ar_name' => $request->ar_name,
                'en_name' => $request->en_name,
                'ar_desc' => $request->ar_desc,
                'en_desc' => $request->en_desc,
                'email' => $request->email,
                'phones' => serialize(array_filter($request->phones)),
                'logo' => $image
            ]
        );

        $admin = ProviderAdmin::create(
            [
                'provider_id' => $provider->id,
                'badge_id' => $request->badge_id,
                'role' => 'system_admin',
                'name' => $provider->en_name,
                'email' => $provider->email,
                'phone' => $request->mobile,
                'username' => $request->username,
                'password' => Hash::make($request->password),
                'image' => $image1
            ]
        );

        $admin->assignRole('provider_system_admin');

        WareHouse::Up($provider->id);
        WareHouseRequest::Up($provider->id);

        $subs = Category::where('type', 2)->pluck('price','id');

        foreach($subs as $id => $fee)
        {
            ProviderCategoryFee::create
            (
                [
                    'provider_id' => $provider->id,
                    'cat_id' => $id,
                    'fee' => $fee
                ]
            );
        }

        return redirect('/admin/providers/active')->with('success', 'Provider added successfully !');
    }


    public function edit($id)
    {
        $provider = Provider::where('id', $id)->with('address')->first();
        $addresses = Address::where('parent_id', NULL)->get();

        return view('admin.providers.single', compact('provider', 'addresses'));
    }


    public function update(Request $request)
    {
        $admin = ProviderAdmin::where('provider_id', $request->provider_id)->first();
        $request->merge(['admin_id' => $admin->id]);

        $this->validate($request,
            [
                'provider_id' => 'required|exists:providers,id',
                'address_id' => 'sometimes',
                'interest_fee' => 'required|numeric',
                'warehouse_fee' => 'required|numeric',
                'ar_name' => 'required|unique:providers,ar_name,'.$request->provider_id,
                'en_name' => 'required|unique:providers,en_name,'.$request->provider_id,
                'ar_desc' => 'required',
                'en_desc' => 'required',
                'email' => 'required|email|unique:providers,email,'.$request->provider_id,
                'phones' => 'required|array',
                'logo' => 'sometimes|image',
                'username' => 'required|unique:provider_admins,username,'.$request->admin_id,
                'password' => 'sometimes|confirmed'
            ],
            [
                'address_id.required' => 'Address is required',
                'interest_fee.required' => 'Interest Fee is required',
                'interest_fee.numeric' => 'Interest Fee is not a number',
                'warehouse_fee.required' => 'Warehouse Fee is required',
                'warehouse_fee.numeric' => 'Warehouse Fee is not a number',
                'ar_name.required' => 'Arabic name is required',
                'ar_name.unique' => 'Arabic name already exists',
                'en_name.required' => 'English name is required',
                'en_name.unique' => 'English name already exists',
                'ar_desc.required' => 'Arabic description is required',
                'en_desc.required' => 'English description is required',
                'email.required' => 'Email is required',
                'email.unique' => 'Email already exists',
                'phones.required' => 'Phones are required',
                'logo.image' => 'Logo is not valid',
                'username.required' => 'Username is required',
                'username.unique' => 'Username already exists',
                'password.required' => 'Password is required',
                'password.confirmed' => 'Password does not match',
            ]
        );

        $provider = Provider::where('id', $request->provider_id)->first();
                if($request->address_id) $provider->address_id = $request->address_id;
                $provider->type = $request->type;
                $provider->interest_fee = $request->interest_fee;
                $provider->warehouse_fee = $request->warehouse_fee;
                $provider->ar_name = $request->ar_name;
                $provider->en_name = $request->en_name;
                $provider->ar_desc = $request->ar_desc;
                $provider->en_desc = $request->en_desc;
                $provider->email = $request->email;
                $provider->phones = serialize(array_filter($request->phones));
                if($request->logo)
                {
                    $image = unique_file($request->logo->getClientOriginalName());
                    $request->logo->move(base_path().'/public/providers/logos/', $image);
                    $provider->logo = $image;
                }
        $provider->save();


            $admin->username = $request->username;
            if($request->password) $admin->password = Hash::make($request->password);
        $admin->save();

        if($provider->active == 1) $text = 'active';
        else $text = 'suspended';

        return redirect('/admin/providers/'.$text)->with('success', 'Provider added successfully !');
    }


    public function change_state(Request $request)
    {
        $this->validate($request,
            [
                'provider_id' => 'required|exists:providers,id',
                'state' => 'required|in:0,1',
            ]
        );

        $provider = Provider::find($request->provider_id);
            $provider->active = $request->state;
        $provider->save();

        if($provider->active == 1)
        {
            return back()->with('success', 'Provider activated successfully !');
        }
        else
        {
            return back()->with('success', 'Provider suspended successfully !');
        }
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'provider_id' => 'required|exists:providers,id',
            ]
        );

        $provider = Provider::where('id',$request->provider_id)->first();

        if($provider->active == 1)
        {
            $provider->delete();
            WareHouse::Down($request->provider_id);
            WareHouseRequest::Down($request->provider_id);

            return redirect('/admin/providers/active')->with('success', 'Provider deleted successfully !');
        }
        elseif($provider->active == 0)
        {
            $provider->delete();
            WareHouse::Down($request->provider_id);
            WareHouseRequest::Down($request->provider_id);

            return redirect('/admin/providers/suspended')->with('success', 'Provider deleted successfully !');
        }
    }


    public function change_password(Request $request)
    {
        $this->validate($request,
            [
                'provider_id' => 'required|exists:providers,id',
                'password' => 'required|min:6|confirmed'
            ],
            [
                'password.required' => 'Password is required',
                'password.min' => 'Password must be 6 digits at minimum',
                'password.confirmed' => 'Password and its confirmation does not match'
            ]
        );

        $provider = Provider::find($request->provider_id);
            $provider->password = Hash::make($request->pasword);
        $provider->save();

        return back()->with('success', 'Password has been changed successfully !');
    }


    public function get_subscriptions($provider_id)
    {
        $provider = Provider::find($provider_id);
        $subscriptions = ProviderSubscription::where('provider_id', $provider_id)->first();

        if(isset($subscriptions))
        {
            $subs = unserialize($subscriptions->subs);
        }
        else
        {
            $subs = [];
        }

        $cats = Category::where('parent_id', NULL)->get();

        return view('admin.providers.subscriptions', compact('provider','subs','cats'));
    }


    public function set_subscriptions(Request $request)
    {
        $this->validate($request,
            [
                'provider_id' => 'required|exists:providers,id',
                'subs' => 'required|array',
                'subs.*' => 'exists:categories,id,type,2'
            ]
        );

        ProviderSubscription::updateOrCreate
        (
            [
                'provider_id' => $request->provider_id
            ],
            [
                'subs' => serialize($request->subs)
            ]
        );

        foreach($request->subs as $sub)
        {
            $check =  ProviderCategoryFee::where('provider_id', $request->provider_id)->where('cat_id', $sub)->first();

            if(! $check)
            {
                $fee = Category::where('id', $sub)->select('price')->first()->price;

                $cat_fee = new ProviderCategoryFee();
                    $cat_fee->provider_id = $request->provider_id;
                    $cat_fee->cat_id = $sub;
                    $cat_fee->fee = $fee;
                $cat_fee->save();
            }
        }

        ProviderCategoryFee::where('provider_id', $request->provider_id)->whereNotIn('cat_id', $request->subs)->delete();

        $state = Provider::find($request->provider_id)->active;

        if($state == 0)
        {
            $state = 'suspended';
        }
        else
        {
            $state = 'active';
        }

        return redirect('/admin/providers/'.$state)->with('success', 'Subscriptions have been set successfully !');
    }

    public function get_sub_company($parent)
    {
        $sub_company = SubCompany::where('parent_id', $parent)->where('status', 'active')->select('id', 'en_name')->get();
        return response()->json($sub_company);
    }

    public function get_sub_category_provider($provider_id,$parent)
    {
        $subs = ProviderSubscription::where('provider_id', $provider_id)->first()->subs;
        $cats = Category::whereIn('id', unserialize($subs))->where('parent_id', $parent)->select('id','en_name')->get();

        return response()->json($cats);
    }

    public function bills($id)
    {
        $provider = Provider::whereId($id)->select('id', 'en_name')->first();

        $collaboration = Collaboration::where('provider_id',$id)->pluck('company_id');
        $companies = Company::whereIn('id', $collaboration)->select('id', 'en_name')->get();

        $subs = ProviderSubscription::where('provider_id', $id)->first()->subs;

        $cat_ids = Category::whereIn('id', unserialize($subs))->pluck('parent_id');
        $cats = Category::whereIn('id', $cat_ids)->select('id','en_name')->get();

        $orders = Order::where('provider_id',$id)->where('completed', 1)->latest()->paginate(50);
        return view('admin.providers.bills.index', compact('orders', 'id', 'provider', 'cats','companies'));

    }

    public function view_bills($id,Request $request)
    {
        $company_id = Collaboration::whereProviderId($id)->select('company_id')->pluck('company_id');
        $provider = Provider::whereId($id)->select('type','interest_fee','warehouse_fee','en_name')->first();

        $get_orders = json_decode($request->order_data);

        $orders = [];
        $total_orders = [];
        $total_items = [];
        foreach ($get_orders as $order)
        {
            $order = Order::whereId($order->id)->first();
            array_push($orders,$order);
            array_push($total_orders,$order->order_total);
            array_push($total_items,$order->item_total);
        }
        $total_orders = array_sum($total_orders);
        $total_items = array_sum($total_items);
        $total_sum =  $total_orders+ $total_items;

        return view('admin.providers.bills.show',
            compact('id','orders','company_id','total_sum','total_orders','total_items','company_name','provider'));
    }

    public function bills_search($id,Request $request)
    {
        $collaboration = Collaboration::where('provider_id',$id)->pluck('company_id');
        $companies = Company::whereIn('id', $collaboration)->select('id', 'en_name')->get();

        $subs = ProviderSubscription::where('provider_id', $id)->first()->subs;
        $cat_ids = Category::whereIn('id', unserialize($subs))->pluck('parent_id');
        $cats = Category::whereIn('id', $cat_ids)->select('id','en_name')->get();

        $search = Input::get('search');

        if($search != '')
        {

            $user = User::whereIn('company_id', $collaboration)->where(function($q) use($search)
            {
                $q->where('en_name','like','%'.$search.'%');
                $q->orWhere('ar_name','like','%'.$search.'%');
            }
            )->first();

            $tech = Technician::where('provider_id', $id)->where(function($q) use($search)
            {
                $q->where('en_name','like','%'.$search.'%');
                $q->orWhere('ar_name','like','%'.$search.'%');
            }
            )->first();

            $orders = Order::where('provider_id', $id)->where('completed', 1);

            if(is_numeric($search))
            {
                $orders = Order::where('provider_id', $id)->where('completed', 1)->where(function($q) use($search)
                {
                    $q->where('id','like','%'.$search.'%');
                    $q->orWhere('smo','like','%'.$search.'%');
                }
                );
            }else
            {
                if($user)
                {
                    $orders = $orders->where('user_id', $user->id);
                }
                if($tech)
                {
                    $orders = $orders->where('tech_id', $tech->id);
                }
            }

            if($request->has('company_id'))
            {
                $orders = $orders->where('company_id', $request->company_id);
            }

            if($request->has('sub_company'))
            {
                $get_sub = User::whereIn('company_id', $collaboration)->whereIn('sub_company_id', $request->sub_company)->pluck('id');
                $orders = $orders->whereIn('user_id', $get_sub);            }

            if($request->from && $request->to)
            {
                $orders = $orders->where('created_at','>=',$request->from)->where('created_at','<=',Carbon::parse($request->to)->addDays(1));
            }

            if($request->has('main_cats'))
            {
                $cat = Category::where('id', $request->main_cats)->first();
                $sub_cat = Category::where('parent_id', $cat->id)->pluck('id');
                $orders = $orders->whereIn('sub_cat_id', $sub_cat);
            }

            if($request->has('sub_cats'))
            {
                $orders = $orders->where('sub_cat_id', $request->sub_cats);
            }

            if($request->has('price_range'))
            {
                $price_range = explode(';',$request->price_range);
                $orders = $orders->where('order_total','>=',$price_range[0])->where('order_total','<=',$price_range[1]);
            }
            if($request->service_type)
            {
                $orders = $orders->whereIn('service_type', $request->service_type);
            }

            $bills_orders = $orders->latest()->get();
            $orders = $orders->latest()->paginate(50);

            $from = $request->from;
            $to = $request->to;
            $sub_company = SubCompany::where('id',$request->sub_company)->first();
            $main_cats = Category::where('id',$request->main_cats)->first();
            $sub_cats = Category::where('id',$request->sub_cats)->first();


        }else{

            $orders = Order::where('provider_id', $id)->where('completed', 1);

            if($request->has('company_id'))
            {
                $orders = $orders->where('company_id', $request->company_id);
            }

            if($request->has('sub_company'))
            {
                $get_sub = User::whereIn('company_id', $collaboration)->whereIn('sub_company_id', $request->sub_company)->pluck('id');
                $orders = $orders->whereIn('user_id', $get_sub);
            }

            if($request->from && $request->to)
            {
                $orders = $orders->where('created_at','>=',$request->from)->where('created_at','<=',Carbon::parse($request->to)->addDays(1));
            }

            if($request->has('main_cats'))
            {
                $cat = Category::where('id', $request->main_cats)->first();
                $sub_cat = Category::where('parent_id', $cat->id)->pluck('id');
                $orders = $orders->whereIn('sub_cat_id', $sub_cat);
            }

            if($request->has('sub_cats'))
            {
                $orders = $orders->where('sub_cat_id', $request->sub_cats);
            }

            if($request->has('price_range'))
            {
                $price_range = explode(';',$request->price_range);
                $orders = $orders->where('order_total','>=',$price_range[0])->where('order_total','<=',$price_range[1]);
            }
            if($request->service_type)
            {
                $orders = $orders->whereIn('service_type', $request->service_type);
            }

            $bills_orders = $orders->latest()->get();
            $orders = $orders->latest()->paginate(50);

            $from = $request->from;
            $to = $request->to;
            $sub_company = SubCompany::where('id',$request->sub_company)->first();
            $main_cats = Category::where('id',$request->main_cats)->first();
            $sub_cats = Category::where('id',$request->sub_cats)->first();
            $company_name = Company::where('id',$request->company_id)->first();
        }

        return view('admin.providers.bills.index', compact('orders', 'id', 'search', 'collaboration', 'cats',
            'from', 'to', 'sub_company', 'main_cats', 'sub_cats','companies','company_name','cats','company','bills_orders'));
    }

    public function bills_all()
    {
        $providers = Provider::where('active', 1)->get();

        return view('admin.providers.bills.show_all',compact('providers'));
    }

    public function bills_all_search(Request $request)
    {
        $providers = Provider::where('active', 1)->get();
        $from = $request->from;
        $to = $request->to;
        return view('admin.providers.bills.show_all',compact('providers','from','to')) ;
    }

    public function date_year_orders($id, $type)
    {
        $collaboration = Collaboration::where('provider_id',$id)->pluck('company_id');
        $companies = Company::whereIn('id', $collaboration)->select('id', 'en_name')->get();

        $subs = ProviderSubscription::where('provider_id', $id)->first()->subs;
        $cat_ids = Category::whereIn('id', unserialize($subs))->pluck('parent_id');
        $cats = Category::whereIn('id', $cat_ids)->select('id','en_name')->get();

        $this_month = new Carbon('first day of this month');
        $this_year = new Carbon('first day of january this year');

        $monthly_orders = Order::raw('table orders')->where('provider_id', $id)->where('created_at','>=', $this_month->toDateTimeString());
        $yearly_orders = Order::raw('table orders')->where('provider_id', $id)->where('created_at','>=', $this_year->toDateTimeString());

        if($type == 'monthly_orders')
        {
            $orders = $monthly_orders;
        }
        elseif($type == 'year_orders')
        {
            $orders = $yearly_orders;
        }
        elseif($type == 'monthly_orders_opened')
        {
            $orders = $monthly_orders->where('completed', 0)->where('canceled', 0);
        }
        elseif($type == 'year_orders_opened')
        {
            $orders = $yearly_orders->where('completed', 0)->where('canceled', 0);
        }
        elseif($type == 'monthly_orders_closed')
        {
            $orders = $monthly_orders->where('completed', 1)->where('canceled', 0);;
        }
        elseif($type == 'year_orders_closed')
        {
            $orders = $yearly_orders->where('completed', 1)->where('canceled', 0);;
        }
        elseif($type == 'monthly_orders_canceled')
        {
            $orders = $monthly_orders->where('canceled', 1);
        }
        elseif($type == 'year_orders_canceled')
        {
            $orders = $yearly_orders->where('canceled', 1);
        }
        elseif($type == 'monthly_parts_orders_count')
        {
            $orders = $monthly_orders->where('type','re_scheduled');
        }
        elseif($type == 'yearly_parts_orders_count')
        {
            $orders = $yearly_orders->where('type','re_scheduled');
        }

        $orders = $orders->latest()->paginate(50);

        return view('admin.orders.provider_orders',
            compact('orders', 'id', 'type','cats','companies'));
    }

    public function date_search($id, $type,Request $request)
    {
        $collaboration = Collaboration::where('provider_id',$id)->pluck('company_id');
        $companies = Company::whereIn('id', $collaboration)->select('id', 'en_name')->get();

        $subs = ProviderSubscription::where('provider_id', $id)->first()->subs;
        $cat_ids = Category::whereIn('id', unserialize($subs))->pluck('parent_id');
        $cats = Category::whereIn('id', $cat_ids)->select('id','en_name')->get();

        $this_year = new Carbon('first day of january this year');
        $this_month = new Carbon('first day of this month');

        $yearly_orders = Order::raw('table orders')->where('provider_id', $id)->where('created_at','>=', $this_year->toDateTimeString());
        $monthly_orders = Order::raw('table orders')->where('provider_id', $id)->where('created_at','>=', $this_month->toDateTimeString());

        $search = Input::get('search');

        if($type == 'monthly_orders')
        {
            $show_orders = $monthly_orders;
        }
        elseif($type == 'year_orders')
        {
            $show_orders = $yearly_orders;
        }
        elseif($type == 'monthly_orders_opened')
        {
            $show_orders = $monthly_orders->where('completed', 0)->where('canceled', 0);
        }
        elseif($type == 'year_orders_opened')
        {
            $show_orders = $yearly_orders->where('completed', 0)->where('canceled', 0);
        }
        elseif($type == 'monthly_orders_closed')
        {
            $show_orders = $monthly_orders->where('completed', 1);
        }
        elseif($type == 'year_ordered_closed')
        {
            $show_orders = $yearly_orders->where('completed', 1);
        }
        elseif($type == 'monthly_orders_canceled')
        {
            $show_orders = $monthly_orders->where('canceled', 1);
        }
        elseif($type == 'year_orders_canceled')
        {
            $show_orders = $yearly_orders->where('canceled', 1);
        }
        elseif($type == 'monthly_parts_orders_count')
        {
            $show_orders = $monthly_orders->where('type','re_scheduled');
        }
        elseif($type == 'yearly_parts_orders_count')
        {
            $show_orders = $yearly_orders->where('type','re_scheduled');
        }

        $get_orders = new Order;
        $orders = $get_orders->search($show_orders,$search,$request->company_id,$id,$request->company_id,
            $request->sub_company,$request->from,$request->to,$request->main_cats,$request->sub_cats,$request->price_range,
            $request->service_type);
        $orders = $orders['orders'];

        return view('admin.orders.provider_orders',
            compact('orders','search', 'id', 'type','companies','cats'));
    }

    public function date_items($id,$type)
    {
        $this_year = new Carbon('first day of january this year');
        $this_month = new Carbon('first day of this month');

        $yearly_orders = Order::raw('table orders')->where('provider_id', $id)->where('created_at','>=', $this_year->toDateTimeString());
        $monthly_orders = Order::raw('table orders')->where('provider_id', $id)->where('created_at','>=', $this_month->toDateTimeString());

        if($type == 'monthly_parts_count')
        {
            $orders = $monthly_orders->where('type','re_scheduled')->latest()->paginate(50);
        }
        elseif($type == 'yearly_parts_count')
        {
            $orders = $yearly_orders->where('type','re_scheduled')->latest()->paginate(50);
        }

        return view('admin.orders.show_items_dashboard',compact('orders','type'));
    }

    public function date_price($id,$type)
    {
        $company_ids = Collaboration::where('provider_id',$id)->pluck('company_id');

        $this_month = new Carbon('first day of this month');
        $this_year = new Carbon('first day of january this year');

        $monthly_orders = Order::raw('table orders')->where('provider_id', $id)->where('created_at','>=', $this_month->toDateTimeString());
        $yearly_orders = Order::raw('table orders')->where('provider_id', $id)->where('created_at','>=', $this_year->toDateTimeString());

        if(strpos($type,'revenue'))
        {
            if($type == 'monthly_revenue')
            {
                $orders = $monthly_orders;
                $total_sum = $monthly_orders->sum('order_total');
            }
            elseif($type == 'yearly_revenue')
            {
                $orders = $yearly_orders;
                $total_sum = $yearly_orders->sum('order_total');
            }
            $orders = $orders->paginate(50);

            return view('admin.orders.price_statistics',compact('orders','company_ids','total_sum'));

        }else{
            $monthly_parts_orders = $monthly_orders->where('type','re_scheduled');
            $yearly_parts_orders = $yearly_orders->where('type','re_scheduled');

            if($type == 'monthly_parts_prices')
            {
                $monthly_parts = OrderTechRequest::whereIn('order_id', $monthly_parts_orders->pluck('id'));
                $monthly_parts_data = $monthly_parts->select('item_id','provider_id')->get();

                $monthly_arr= [];
                foreach($monthly_parts_data as $part)
                {
                    $price = DB::table($part->provider_id.'_warehouse_parts')->where('id', $part->item_id)->select('price')->first()->price;
                    array_push($monthly_arr, $price);
                }
                $total_sum = array_sum($monthly_arr);

                $orders = $monthly_parts_orders;
            }
            elseif($type == 'yearly_parts_prices')
            {
                $yearly_parts = OrderTechRequest::whereIn('order_id', $yearly_parts_orders->pluck('id'));
                $yearly_parts_data = $yearly_parts->select('item_id','provider_id')->get();

                $yearly_arr= [];
                foreach($yearly_parts_data as $part)
                {
                    $price = DB::table($part->provider_id.'_warehouse_parts')->where('id', $part->item_id)->select('price')->first()->price;
                    array_push($yearly_arr, $price);
                }
                $total_sum = array_sum($yearly_arr);

                $orders = $yearly_parts_orders;
            }
            $orders = $orders->paginate(50);

            return view('admin.orders.item_statistics',compact('orders','company_ids','total_sum'));}
    }

    public function date_rate($id,$type)
    {
        //month
        $this_month = new Carbon('first day of this month');
        $monthly_orders = Order::raw('table orders')->where('provider_id', $id)->where('created_at','>=', $this_month->toDateTimeString())->get();
        $monthly_orders_ids = $monthly_orders->pluck('id');
        $monthly_rates_ids = OrderRate::whereIn('order_id', $monthly_orders_ids)->pluck('order_id');

        //year
        $this_year = new Carbon('first day of january this year');
        $yearly_orders = Order::raw('table orders')->where('provider_id', $id)->where('created_at','>=', $this_year->toDateTimeString())->get();
        $yearly_orders_ids = $yearly_orders->pluck('id');
        $yearly_rates_ids = OrderRate::whereIn('order_id', $yearly_orders_ids)->pluck('order_id');

        if($type == 'monthly_rate'){
            $orders =  Order::whereIn('id', $monthly_rates_ids)->get();
        }elseif($type == 'yearly_rate'){
            $orders = Order::whereIn('id', $yearly_rates_ids)->get();
        }
        return view('admin.orders.rate_dashboard', compact('orders', 'type'));
    }
}
