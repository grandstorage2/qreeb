<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PushNotify extends Model
{
    public static function user_send($tokens,$ar_text,$en_text,$type,$order_id=null,$extra=null)
    {
        $fields = array
        (
            "registration_ids" => $tokens,
            "priority" => 10,
            'data' => [
                'type' => $type,
                'ar_text' => $ar_text,
                'en_text' => $en_text,
                'order_id' => $order_id,
                'extra' => $extra
            ],
            'notification' => [
                'type' => $type,
                'ar_text' => $ar_text,
                'en_text' => $en_text,
                'order_id' => $order_id,
                'extra' => $extra
            ],
            'vibrate' => 1,
            'sound' => 1
        );
        $headers = array
        (
            'accept: application/json',
            'Content-Type: application/json',
            'Authorization: key=' .
            'AAAAFOTxsqE:APA91bFdKFaupGSZTJgvAhp6grTARJxBOg54lH3juQZu2ZRCIoCYcQiIgxDvfiCTmXZQIgyvkYLlyP5S-pyVhikcaq5leLkYlBkblNx8sSeNl9OndTVz8wQbmfPXjiZ2aVrlt0ufj58u'

        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        //  var_dump($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);

        return $result;
    }


    public static function tech_send($tokens,$ar_text,$en_text,$type,$order_id=null,$extra=null)
    {
        $fields = array
        (
            "registration_ids" => $tokens,
            "priority" => 10,
            'data' => [
                'type' => $type,
                'ar_text' => $ar_text,
                'en_text' => $en_text,
                'order_id' => $order_id,
                'extra' => $extra
            ],
            'notification' => [
                'type' => $type,
                'ar_text' => $ar_text,
                'en_text' => $en_text,
                'order_id' => $order_id,
                'extra' => $extra
            ],
            'vibrate' => 1,
            'sound' => 1
        );
        $headers = array
        (
            'accept: application/json',
            'Content-Type: application/json',
            'Authorization: key=' .
            'AAAAFOTxsqE:APA91bFdKFaupGSZTJgvAhp6grTARJxBOg54lH3juQZu2ZRCIoCYcQiIgxDvfiCTmXZQIgyvkYLlyP5S-pyVhikcaq5leLkYlBkblNx8sSeNl9OndTVz8wQbmfPXjiZ2aVrlt0ufj58u'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        //  var_dump($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);

        return $result;
    }
}
