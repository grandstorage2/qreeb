<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model implements Authenticatable
{
    use AuthenticableTrait;

    protected $fillable =
        [
        'address_id','type','interest_fee','warehouse_fee','ar_name','en_name','ar_desc','en_desc','email','phones','logo','username','password'
        ];


    public function address()
    {
        return $this->belongsTo(Address::class, 'address_id');
    }


    public function technicians()
    {
        return $this->hasMany(Technician::class, 'provider_id');
    }


    public function orders()
    {
        return $this->hasMany(Order::class, 'provider_id');
    }

    public function orders_with_date($from,$to)
    {
        return $this->hasMany(Order::class, 'provider_id')->where('created_at','>=',$from)
        ->where('created_at','<=',Carbon::parse($to)->addDays(1));

    }

    public function orders_urgent()
    {
        return $this->hasMany(Order::class, 'provider_id')->where('type', 'urgent');
    }

    public function orders_scheduled()
    {
        return $this->hasMany(Order::class, 'provider_id')->where('type', 'scheduled');
    }


    public function cat_fees()
    {
        return $this->hasMany(ProviderCategoryFee::class, 'provider_id');
    }


    public function admin()
    {
        return $this->hasOne(ProviderAdmin::class,'provider_id');
    }
}
