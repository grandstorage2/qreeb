<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Middleware\IsAdmin;
use App\Http\Middleware\IsProvider;
use App\Http\Middleware\IsCompany;

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => '/admin'], function(){

    Route::get('/login', 'Admin\AuthController@login_view');
    Route::post('/login', 'Admin\AuthController@login');
    Route::get('/logout', 'Admin\AuthController@logout');

    Route::group(['middleware' => ['web', IsAdmin::class]], function () {

        Route::get('/dashboard', 'Admin\HomeController@dashboard');
        Route::get('/profile', 'Admin\HomeController@profile');
        Route::post('/profile/update', 'Admin\HomeController@update_profile');
        Route::post('/change_password', 'Admin\HomeController@change_password');

        Route::group(['middleware' => ['permission:admins_operate']], function ()
        {
            Route::get('/admins/{type}/index', 'Admin\AdminController@index');
            Route::get('/admins/search', 'Admin\AdminController@search');
            Route::get('/admin/create', 'Admin\AdminController@create');
            Route::post('/admin/store', 'Admin\AdminController@store');
            Route::get('/admins/{id}/view', 'Admin\AdminController@show');
            Route::get('/admin/{id}/edit', 'Admin\AdminController@edit');
            Route::post('/admin/update', 'Admin\AdminController@update');
            Route::post('/admin/delete', 'Admin\AdminController@destroy');
            Route::post('/admin/change_status', 'Admin\AdminController@change_status');

        });



        Route::group(['middleware' => ['permission:addresses_observe']], function ()
        {
            Route::get('/addresses/search', 'Admin\AddressController@search');
            Route::get('/addresses/{parent}', 'Admin\AddressController@index');
        });

        Route::group(['middleware' => ['permission:addresses_operate']], function ()
        {
            Route::get('/address/country/create', 'Admin\AddressController@country_create');
            Route::get('/address/city/create', 'Admin\AddressController@city_create');
            Route::post('/address/store', 'Admin\AddressController@store');
            Route::get('/address/{id}/edit', 'Admin\AddressController@edit');
            Route::post('/address/update', 'Admin\AddressController@update');
            Route::post('/address/delete', 'Admin\AddressController@destroy');
        });



        Route::group(['middleware' => ['permission:categories_observe']], function ()
        {
            Route::get('/categories/search', 'Admin\CategoryController@search');
            Route::get('/categories/{parent}', 'Admin\CategoryController@index');
        });

        Route::group(['middleware' => ['permission:categories_operate']], function ()
        {
            Route::get('/categories/excel/export', 'Admin\CategoryController@excel_export');
            Route::get('/categories/excel/export/sub_category', 'Admin\CategoryController@excel_export_sub');
            Route::get('/categories/excel/export/sub_category/sub', 'Admin\CategoryController@excel_export_thirdparty');
            Route::get('/category/main/create', 'Admin\CategoryController@main_create');
            Route::get('/category/sub/create', 'Admin\CategoryController@sub_create');
            Route::get('/category/secondary/create', 'Admin\CategoryController@sec_create');
            Route::post('/category/main_store', 'Admin\CategoryController@main_store');
            Route::post('/category/sub_store', 'Admin\CategoryController@sub_store');
            Route::post('/category/sec_store', 'Admin\CategoryController@sec_store');
            Route::get('/category/{id}/main_edit', 'Admin\CategoryController@main_edit');
            Route::get('/category/{id}/sub_edit', 'Admin\CategoryController@sub_edit');
            Route::get('/category/{id}/sec_edit', 'Admin\CategoryController@sec_edit');
            Route::post('/category/main_update', 'Admin\CategoryController@main_update');
            Route::post('/category/sub_update', 'Admin\CategoryController@sub_update');
            Route::post('/category/sec_update', 'Admin\CategoryController@sec_update');
            Route::post('/category/delete', 'Admin\CategoryController@destroy');
        });



        Route::group(['middleware' => ['permission:providers_observe']], function ()
        {
            Route::get('/providers/search', 'Admin\ProviderController@search');
            Route::get('/providers/{state}', 'Admin\ProviderController@index');
            Route::get('/provider/{id}/view', 'Admin\ProviderController@show');
            Route::get('/provider/{id}/bills', 'Admin\ProviderController@bills');
            Route::get('/provider/{id}/bills/search', 'Admin\ProviderController@bills_search');
            Route::post('/provider/{id}/bills/view', 'Admin\ProviderController@view_bills');
            Route::get('/provider/bills/all','Admin\ProviderController@bills_all');
            Route::get('/provider/bills/all/search','Admin\ProviderController@bills_all_search');
            Route::get('/provider/{company_id}/get_sub_category_provider/{parent}', 'Admin\ProviderController@get_sub_category_provider');
            Route::get('/provider/get_sub_company/{parent}', 'Admin\ProviderController@get_sub_company');

        });

        Route::group(['middleware' => ['permission:providers_observe_statistics']], function ()
        {
            Route::get('/provider/{id}/statistics', 'Admin\ProviderController@statistics');
            Route::get('/provider/{id}/statistics/{type}', 'Admin\ProviderController@date_year_orders');
            Route::get('/provider/{id}/statistics/{type}/search', 'Admin\ProviderController@date_search');
            Route::get('/provider/{id}/statistics/items/{type}', 'Admin\ProviderController@date_items');
            Route::get('/provider/{id}/statistics/price/{type}', 'Admin\ProviderController@date_price');
            Route::get('/provider/{id}/statistics/rate/{type}', 'Admin\ProviderController@date_rate');
        });

        Route::group(['middleware' => ['permission:providers_subscriptions']], function ()
        {
            Route::post('/provider/subscriptions', 'Admin\ProviderController@set_subscriptions');
        });

        Route::group(['middleware' => ['permission:providers_operate']], function ()
        {
            Route::get('/provider/create', 'Admin\ProviderController@create');
            Route::post('/provider/store', 'Admin\ProviderController@store');
            Route::get('/provider/{id}/edit', 'Admin\ProviderController@edit');
            Route::post('/provider/update', 'Admin\ProviderController@update');
            Route::post('/provider/change_state', 'Admin\ProviderController@change_state');
            Route::post('/provider/delete', 'Admin\ProviderController@destroy');
            Route::post('/provider/change_password', 'Admin\ProviderController@change_password');
            Route::get('/provider/{id}/subscriptions', 'Admin\ProviderController@get_subscriptions');
        });



        Route::group(['middleware' => ['permission:companies_observe']], function ()
        {
            Route::get('/companies/search', 'Admin\CompanyController@search');
            Route::get('/companies/{state}', 'Admin\CompanyController@index');
            Route::get('/company/{id}/view', 'Admin\CompanyController@show');
        });

        Route::group(['middleware' => ['permission:companies_observe_statistics']], function ()
        {
            Route::get('/company/{id}/statistics', 'Admin\CompanyController@statistics');
            Route::get('/company/{id}/statistics/{type}', 'Admin\CompanyController@date_year_orders');
            Route::get('/company/{id}/statistics/{type}/search', 'Admin\CompanyController@date_search');
            Route::get('/company/{id}/statistics/items/{type}', 'Admin\CompanyController@date_items');
            Route::get('/company/{id}/statistics/price/{type}', 'Admin\CompanyController@date_price');
            Route::get('/company/{id}/statistics/rate/{type}', 'Admin\CompanyController@date_rate');
        });

        Route::group(['middleware' => ['permission:companies_subscriptions']], function ()
        {
            Route::get('/company/{id}/subscriptions', 'Admin\CompanyController@get_subscriptions');
            Route::post('/company/subscriptions', 'Admin\CompanyController@set_subscriptions');
        });

        Route::group(['middleware' => ['permission:companies_operate']], function ()
        {
            Route::get('/company/create', 'Admin\CompanyController@create');
            Route::post('/company/store', 'Admin\CompanyController@store');
            Route::get('/company/{id}/edit', 'Admin\CompanyController@edit');
            Route::post('/company/update', 'Admin\CompanyController@update');
            Route::post('/company/delete', 'Admin\CompanyController@destroy');
            Route::post('/company/change_state', 'Admin\CompanyController@change_state');
        });



        Route::group(['middleware' => ['permission:collaborations_observe']], function ()
        {
            Route::get('/collaborations', 'Admin\CollaborationController@index');
            Route::get('/collaborations/search', 'Admin\CollaborationController@search');

            Route::get('/orders/dashboard/{type}','Admin\HomeController@date_orders');
            Route::get('/orders/{type}','Admin\HomeController@date_re_orders');
            Route::get('/items/dashboard/{type}', 'Admin\HomeController@date_items');
            Route::get('/price/dashboard/{type}', 'Admin\HomeController@date_price');
            Route::get('/orders/{type}/search', 'Admin\HomeController@search');
            Route::get('/order/{id}/view', 'Admin\HomeController@show');
            Route::get('/rate/{type}', 'Admin\HomeController@data_rate_orders');
        });
//        Route::get('/orders/dashboard/{type}', 'Provider\HomeController@date_orders');
//        Route::get('/items/dashboard/{type}', 'Provider\HomeController@date_items');
//        Route::get('/price/dashboard/{type}', 'Provider\HomeController@date_price');
//        Route::get('/{type}/search', 'Provider\HomeController@search');
//        Route::get('/rate/{type}', 'Provider\HomeController@data_rate_orders');

        Route::group(['middleware' => ['permission:collaborations_operate']], function ()
        {
            Route::get('/collaboration/create', 'Admin\CollaborationController@create');
            Route::post('/collaboration/store', 'Admin\CollaborationController@store');
            Route::get('/collaboration/{provider_id}/edit', 'Admin\CollaborationController@edit');
            Route::post('/collaboration/update', 'Admin\CollaborationController@update');
            Route::post('/collaboration/delete', 'Admin\CollaborationController@destroy');
        });

        Route::get('/individuals/user/{state}', 'Admin\IndividualController@user_index');
        Route::get('/individual/user/create', 'Admin\IndividualController@user_create');
        Route::post('/individual/user/store', 'Admin\IndividualController@user_store');
        Route::get('/individual/user/{id}/view', 'Admin\IndividualController@user_show');
        Route::get('/individual/user/{id}/edit', 'Admin\IndividualController@user_edit');
        Route::post('/individual/user/update', 'Admin\IndividualController@user_update');
        Route::post('/individual/user/change_state', 'Admin\IndividualController@user_change_status');
        Route::post('/individual/user/change_password', 'Admin\IndividualController@user_change_password');
        Route::post('/individual/user/delete', 'Admin\IndividualController@user_destroy');


        Route::get('/individuals/technician/{state}', 'Admin\IndividualController@index');
        Route::get('/individual/technician/create', 'Admin\IndividualController@create');
        Route::post('/individual/technician/store', 'Admin\IndividualController@store');
        Route::get('/individual/technician/{id}/view', 'Admin\IndividualController@show');
        Route::get('/individual/technician/{id}/edit', 'Admin\IndividualController@edit');
        Route::post('/individual/technician/update', 'Admin\IndividualController@update');
        Route::post('/individual/technician/change_state', 'Admin\IndividualController@change_status');
        Route::post('/individual/technician/change_password', 'Admin\IndividualController@change_password');
        Route::post('/individual/technician/delete', 'Admin\IndividualController@destroy');

        Route::get('/users/{state}', 'Admin\UserController@index');
        Route::get('/user/create', 'Admin\UserController@create');

        Route::group(['prefix' => '/settings'], function ()
        {
            Route::group(['middleware' => ['permission:settings_observe']], function ()
            {
                Route::get('/about', 'Admin\AboutController@index');
                Route::get('/terms', 'Admin\AboutController@terms');
                Route::get('/privacy', 'Admin\AboutController@privacy');
                Route::get('/complains', 'Admin\AboutController@complains');
            });

            Route::group(['middleware' => ['permission:settings_operate']], function ()
            {
                Route::get('/about/edit', 'Admin\AboutController@edit');
                Route::post('/about/update', 'Admin\AboutController@update');

                Route::get('/terms/edit', 'Admin\AboutController@terms_edit');
                Route::post('/terms/update', 'Admin\AboutController@terms_update');

                Route::get('/privacy/edit', 'Admin\AboutController@privacy_edit');
                Route::post('/privacy/update', 'Admin\AboutController@privacy_update');
            });

//            Route::get('/notifications', 'Admin\NotifyController@index');
//            Route::post('/notification/store', 'Admin\NotifyController@store');
//            Route::get('/notification/delete/{id}', 'Admin\NotifyController@destroy');
        });

        //Ajax Routes
        Route::get('/get_cities/{parent}', 'Admin\HomeController@get_cities');
        Route::get('/get_sub_cats/{parent}', 'Admin\HomeController@get_sub_cats');
        //End Ajax Routes

        //Mail Routes
        Route::post('/mail/send', 'Admin\MailController@send');
        //End Mail Routes
    });
});


Route::group(['prefix' => '/provider'], function(){

    Route::get('/login', 'Provider\AuthController@login_view');
    Route::post('/login', 'Provider\AuthController@login');

    Route::group(['middleware' => ['web', IsProvider::class]], function () {

        Route::get('/dashboard', 'Provider\HomeController@dashboard');
        Route::get('/profile', 'Provider\HomeController@profile');
        Route::post('/profile/update', 'Provider\HomeController@update_profile');
        Route::post('/change_password', 'Provider\HomeController@change_password');
        Route::get('/info', 'Provider\HomeController@info');
        Route::post('/info/update', 'Provider\HomeController@update_info');
        Route::get('/logout', 'Provider\AuthController@logout');

        Route::get('/collaborations', 'Provider\CollaborationController@index');
        Route::get('/collaboration/{id}/statistics', 'Provider\CollaborationController@statistics');
        Route::get('/collaboration/{id}/bills', 'Provider\CollaborationController@bills');
        Route::post('/collaboration/{company_id}/bills/view', 'Provider\CollaborationController@view_bills');
        Route::get('/collaboration/{id}/bills/search', 'Provider\CollaborationController@bills_search');
        Route::get('/collaboration/{id}/bills_export', 'Provider\CollaborationController@bills_export');
        Route::get('/collaboration/{id}/bills_export/{search}', 'Provider\CollaborationController@bills_export_search');
        Route::get('/collaboration/{id}/statistics/{type}', 'Provider\CollaborationController@date_year_orders');
        Route::get('/collaboration/{id}/statistics/{type}/search', 'Provider\CollaborationController@search');
        Route::get('/collaboration/{id}/statistics/items/{type}', 'Provider\CollaborationController@date_items');
        Route::get('/collaboration/{id}/statistics/price/{type}', 'Provider\CollaborationController@date_price');
        Route::get('/collaboration/{id}/statistics/rate/{type}', 'Provider\CollaborationController@date_rate');
        Route::get('/collaboration/{collaboration_id}/order/{id}/view', 'Provider\CollaborationController@show');
        Route::get('/get_sub_company/{parent}', 'Provider\CollaborationController@get_sub_company');
        Route::get('/{company_id}/get_sub_category_provider/{parent}', 'Provider\CollaborationController@get_sub_category_provider');
//        Route::get('/collaboration/{company_id}/print/bills', 'Provider\CollaborationController@print_bills');

        Route::get('/technician/excel/view', 'Provider\TechnicianController@excel_view');
        Route::post('/technician/excel/upload', 'Provider\TechnicianController@excel_upload');
        Route::get('/technician/{state}/excel/export', 'Provider\TechnicianController@excel_export');
        Route::get('/technician/images/view', 'Provider\TechnicianController@images_view');
        Route::post('/technician/images/upload', 'Provider\TechnicianController@images_upload');
        Route::get('/technicians/statistics', 'Provider\TechnicianController@statistics');
        Route::get('/technician/{id}/orders/request', 'Provider\TechnicianController@orders_request');
        Route::post('/technician/orders/invoice/show', 'Provider\TechnicianController@orders_show');
        Route::post('/technician/orders/invoice/export', 'Provider\TechnicianController@orders_export');
        Route::get('/technicians/statistics/search', 'Provider\TechnicianController@statistics_search');
        Route::get('/technicians/{state}/search', 'Provider\TechnicianController@search');
        Route::get('/technicians/{state}', 'Provider\TechnicianController@index');
        Route::get('/technician/create', 'Provider\TechnicianController@create');
        Route::post('/technician/store', 'Provider\TechnicianController@store');
        Route::get('/technician/{id}/view', 'Provider\TechnicianController@show');
        Route::get('/technician/{id}/edit', 'Provider\TechnicianController@edit');
        Route::post('/technician/update', 'Provider\TechnicianController@update');
        Route::post('/technician/change_state', 'Provider\TechnicianController@change_state');
        Route::post('/technician/change_password', 'Provider\TechnicianController@change_password');
//        Route::post('/technician/delete', 'Provider\TechnicianController@destroy');

        Route::get('/rotations/index', 'Provider\RotationController@index');
        Route::get('/rotation/create', 'Provider\RotationController@create');
        Route::post('/rotation/store', 'Provider\RotationController@store');
        Route::get('/rotation/{id}/edit', 'Provider\RotationController@edit');
        Route::post('/rotation/update', 'Provider\RotationController@update');
        Route::post('/rotation/delete', 'Provider\RotationController@destroy');


        Route::get('/warehouse/excel/view', 'Provider\WarehouseController@excel_view');
        Route::post('/warehouse/excel/upload', 'Provider\WarehouseController@excel_upload');
        Route::get('/warehouse/images/view', 'Provider\WarehouseController@images_view');
        Route::post('/warehouse/images/upload', 'Provider\WarehouseController@images_upload');
        Route::get('/warehouse/excel/categories/export', 'Provider\WarehouseController@categories_excel_export');
        Route::get('/warehouse/excel/parts/export', 'Provider\WarehouseController@parts_excel_export');
        Route::get('/warehouse/search', 'Provider\WarehouseController@search');
        Route::get('/warehouse/{parent}', 'Provider\WarehouseController@index');
        Route::get('/warehouse/{parent}/items', 'Provider\WarehouseController@items');
        Route::get('/warehouse/item/create', 'Provider\WarehouseController@create');
        Route::post('/warehouse/item/store', 'Provider\WarehouseController@store');
        Route::get('/warehouse/item/{id}/edit', 'Provider\WarehouseController@edit');
        Route::post('/warehouse/item/update', 'Provider\WarehouseController@update');
        Route::post('/warehouse/item/change_status', 'Provider\WarehouseController@change_status');
//        Route::post('/warehouse/item/delete', 'Provider\WarehouseController@destroy');

        Route::get('/warehouse_requests', 'Provider\WarehouseRequestController@index');
//        Route::post('/warehouse_request/delete', 'Provider\WarehouseRequestController@destroy');

        Route::get('/orders/{type}/invoice/request', 'Provider\OrderController@orders_request');
        Route::post('/orders/invoice/show', 'Provider\OrderController@orders_show');
        Route::post('/orders/invoice/export', 'Provider\OrderController@orders_export');
        Route::get('/orders/{state}/search', 'Provider\OrderController@search');
        Route::get('/orders/{type}', 'Provider\OrderController@index');
        Route::get('/orders/open/waiting', 'Provider\OrderController@waiting');
        Route::get('/orders/open/waiting/upload/view', 'Provider\OrderController@waiting_upload_view');
        Route::post('/orders/open/waiting/upload', 'Provider\OrderController@waiting_upload');
        Route::get('/order/{id}/view', 'Provider\OrderController@show');
        Route::post('/order/cancel/{type}', 'Provider\OrderController@cancel');
        Route::get('/orders/excel/view', 'Provider\OrderController@excel_view');
        Route::post('/orders/excel/upload', 'Provider\OrderController@excel_upload');
        Route::get('/orders/excel/tech/view', 'Provider\OrderController@excel_tech_request_view');
        Route::post('/orders/excel/tech/upload', 'Provider\OrderController@excel_tech_request_upload');
        Route::get('/orders/images/view', 'Provider\OrderController@images_view');
        Route::post('/orders/images/upload', 'Provider\OrderController@images_upload');
        Route::get('get_sub_category_provider/{parent}', 'Provider\OrderController@get_sub_category_provider');


        Route::get('/order/{id}/company/statistics/view', 'Provider\OrderController@show');

        Route::get('/collaboration/{id}/services/fees/view', 'Provider\ServiceFeeController@view');
        Route::post('/collaboration/services/fees/update', 'Provider\ServiceFeeController@update');

        Route::get('/collaboration/{id}/third/fees/view', 'Provider\ThirdCategoryFeeController@view');
        Route::post('/collaboration/third/fees/update', 'Provider\ThirdCategoryFeeController@update');

        //Start dashboard month and year orders
        Route::get('/orders/dashboard/{type}', 'Provider\HomeController@date_orders');
        Route::get('/items/dashboard/{type}', 'Provider\HomeController@date_items');
        Route::get('/price/dashboard/{type}', 'Provider\HomeController@date_price');
        Route::get('/{type}/search', 'Provider\HomeController@search');
        Route::get('/rate/{type}', 'Provider\HomeController@data_rate_orders');
        //End dashboard year orders

        //Ajax Routes
        Route::get('/get_cities/{parent}', 'Provider\HomeController@get_cities');
        Route::get('/get_sub_cats/{parent}', 'Provider\HomeController@get_sub_cats');
        //End Ajax Routes
    });
});


Route::group(['prefix' => '/company'], function(){
    Route::get('/login', 'Company\AuthController@login_view');
    Route::post('/login', 'Company\AuthController@login');

    Route::group(['middleware' => ['web', IsCompany::class]], function () {
        Route::get('/dashboard', 'Company\HomeController@dashboard');
        Route::get('/profile', 'Company\HomeController@profile');
        Route::post('/profile/update', 'Company\HomeController@update_profile');
        Route::post('/change_password', 'Company\HomeController@change_password');

        Route::get('/logout', 'Company\AuthController@logout');

        Route::group(['middleware' => ['permission:companies_observe']], function ()
        {
            Route::get('/my_company', 'Company\HomeController@my_company');
        });

        Route::group(['middleware' => ['permission:companies_operate']], function ()
        {
            Route::get('/info', 'Company\HomeController@info');
            Route::post('/info/update', 'Company\HomeController@update_info');
        });


        Route::group(['middleware' => ['permission:admins']], function ()
        {
            Route::get('/admins/{type}/index', 'Company\AdminController@index');
            Route::get('/admins/search', 'Company\AdminController@search');
            Route::get('/admins/{id}/view', 'Company\AdminController@show');
            Route::get('/admin/create', 'Company\AdminController@create');
            Route::post('/admin/store', 'Company\AdminController@store');
            Route::get('/admin/{id}/edit', 'Company\AdminController@edit');
            Route::post('/admin/update', 'Company\AdminController@update');
            Route::post('/admin/delete', 'Company\AdminController@destroy');
            Route::post('/admin/change_status', 'Company\AdminController@change_status');
        });


        Route::group(['middleware' => ['permission:sub_companies_observe']], function () {
            Route::get('/sub_company/{status}/search', 'Company\SubCompanyController@search');
            Route::get('/sub_companies/{state}', 'Company\SubCompanyController@index');
            Route::get('/sub_company/{id}/users', 'Company\SubCompanyController@users');
//        Route::post('/sub_company/delete', 'Company\SubCompanyController@destroy');
        });

        Route::group(['middleware' => ['permission:sub_companies_operate']], function ()
        {
            Route::get('/sub_company/create', 'Company\SubCompanyController@create');
            Route::post('/sub_company/store', 'Company\SubCompanyController@store');
            Route::get('/sub_company/{id}/edit', 'Company\SubCompanyController@edit');
            Route::post('/sub_company/update', 'Company\SubCompanyController@update');
            Route::post('/sub_company/status/change', 'Company\SubCompanyController@change_status');
        });


        Route::group(['middleware' => ['permission:collaborations_observe']], function ()
        {
            Route::get('/collaborations', 'Company\CollaborationController@index');
        });

        Route::group(['middleware' => ['permission:collaborations_operate']], function ()
        {
            Route::get('/collaboration/{collaboration_id}/statistics', 'Company\CollaborationController@statistics');
            Route::get('/collaboration/{provider_id}/orders/request', 'Company\CollaborationController@orders_request');
            Route::post('/collaboration/orders/invoice/show', 'Company\CollaborationController@orders_show');
            Route::post('/collaboration/orders/invoice/export', 'Company\CollaborationController@orders_export');
            Route::get('/collaboration/{provider_id}/fees/show', 'Company\CollaborationController@fees_show');
            Route::get('/collaboration/{provider_id}/fees/export', 'Company\CollaborationController@fees_export');
            Route::get('/collaboration/{id}/statistics/{type}', 'Company\CollaborationController@date_year_orders');
            Route::get('/collaboration/{id}/statistics/{type}/search', 'Company\CollaborationController@search');
            Route::get('/collaboration/{id}/statistics/items/{type}', 'Company\CollaborationController@date_items');
            Route::get('/collaboration/{id}/statistics/price/{type}', 'Company\CollaborationController@date_price');
            Route::get('/collaboration/{id}/statistics/rate/{type}', 'Company\CollaborationController@date_rate');
            Route::get('/collaboration/{collaboration_id}/order/{id}/view', 'Company\CollaborationController@show');
        });


        Route::group(['middleware' => ['permission:users_observe']], function () {

            Route::get('/users/{state}/search', 'Company\UserController@search');
            Route::get('/users/{state}', 'Company\UserController@index');
            Route::get('/user/{id}/view', 'Company\UserController@show')->where(['id' => '[0-9]+']);

        });


            Route::get('/user/excel/view', 'Company\UserController@excel_view');
            Route::post('/user/excel/upload', 'Company\UserController@excel_upload');
            Route::get('/user/images/view', 'Company\UserController@images_view');
            Route::post('/user/images/upload', 'Company\UserController@images_upload');
            Route::get('/users/{state}/excel/export', 'Company\UserController@excel_export');



        Route::group(['middleware' => ['permission:users_operate']], function ()
        {
            Route::get('/user/{id}/orders/request', 'Company\UserController@orders_request');
            Route::post('/user/orders/invoice/show', 'Company\UserController@orders_show');
            Route::post('/user/orders/invoice/export', 'Company\UserController@orders_export');
            Route::get('/user/create', 'Company\UserController@create');
            Route::post('/user/store', 'Company\UserController@store');
            Route::post('/user/order/store', 'Company\UserController@order_store');
            Route::get('/user/{id}/edit', 'Company\UserController@edit');
            Route::post('/user/update', 'Company\UserController@update');
            Route::post('/user/change_state', 'Company\UserController@change_state');
            Route::post('/user/change_password', 'Company\UserController@change_password');
            //        Route::post('/user/delete', 'Company\UserController@destroy');

        });


        Route::group(['middleware' => ['permission:orders_observe']], function ()
        {

            Route::get('/orders/dashboard/{type}', 'Company\HomeController@date_orders');
            Route::get('/items/dashboard/{type}', 'Company\HomeController@date_items');
            Route::get('/price/dashboard/{type}', 'Company\HomeController@date_price');
            Route::get('/{type}/search', 'Company\HomeController@search');
            Route::get('/rate/{type}', 'Company\HomeController@data_rate_orders');


            Route::get('/orders/{type}/search', 'Company\OrderController@search');
            Route::get('/orders/{type}', 'Company\OrderController@index');
            Route::get('/orders/{type}/monthly_orders', 'Company\OrderController@index');
            Route::get('/orders/{type}/monthly_open', 'Company\OrderController@index');
            Route::get('/orders/{type}/monthly_closed', 'Company\OrderController@index');
            Route::get('/order/{id}/view', 'Company\OrderController@show');
            Route::get('/orders/{type}/excel/view', 'Company\OrderController@excel_view');
            Route::post('/orders/{type}/excel/upload', 'Company\OrderController@excel_upload');
            Route::get('/orders/open/{type}/excel/view', 'Company\OrderController@excel_open_view');
            Route::post('/orders/open/{type}/excel/upload', 'Company\OrderController@excel_open_upload');

//            Route::get('/bills', 'Company\OrderController@bills');
//            Route::get('/bills/excel/export', 'Company\OrderController@bills_export');


//            Route::get('/{type}', 'Company\HomeController@date_year_orders');
//            Route::get('/{type}/search', 'Company\HomeController@search');
//            Route::get('/rate/{type}', 'Company\HomeController@data_rate_orders');
        });

        Route::group(['middleware' => ['permission:orders_operate']], function ()
        {
            Route::get('/user/{id}/order/create', 'Company\UserController@order_create');
            Route::get('/orders/{type}/invoice/request', 'Company\OrderController@orders_request');
            Route::post('/orders/invoice/show', 'Company\OrderController@orders_show');
            Route::post('/orders/invoice/export', 'Company\OrderController@orders_export');
        });



//        Route::group(['middleware' => ['permission:item_requests_observe']], function ()

            Route::get('/show/item_requests/search', 'Company\ItemRequestController@search');
            Route::get('/show/item_requests/{status}', 'Company\ItemRequestController@index');

//        Route::group(['middleware' => ['permission:item_requests_operate']], function ()

            Route::post('/change/item_request/change_status', 'Company\ItemRequestController@change_status');


        //Ajax Routes
        Route::get('/get_cities/{parent}', 'Company\HomeController@get_cities');
        Route::get('/get_sub_cats_company/{parent}', 'Company\HomeController@get_subs');
        Route::get('/get_technician/{parent}', 'Company\HomeController@get_technician');
        //End Ajax Routes
    });
});


//Cron Jobs
    Route::get('/crone/schedule', 'Admin\CroneController@schedule');
    Route::get('/crone/rotate', 'Admin\CroneController@rotate');
//End Crone Jobs

Route::get('/policy', 'Admin\HomeController@policy');
