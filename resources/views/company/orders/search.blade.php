@extends('company.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/company/dashboard">Dashboard</a></li>
        <li><a href="{{Request::url()}}">Orders</a></li>
        <li class="active">Search</li>
    </ul>
    <!-- END BREADCRUMB -->

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('company.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    {{--<div class="panel-heading">--}}
                        {{--<a href="/company/order/create"><button type="button" class="btn btn-info"> Make an order </button></a>--}}
                    {{--</div>--}}

                    <form class="form-horizontal" method="get" @if(Request::is('company/orders/urgent/search')) action="/company/orders/urgent/search"
                          @elseif((Request::is('company/orders/scheduled/search'))) action="/company/orders/scheduled/search"
                          @elseif ((Request::is('company/orders/re_scheduled/search'))) action="/company/orders/re_scheduled/search"
                          @else action="/company/orders/canceled/search" @endif>
                        <div class="form-group">
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group" style="margin-top: 10px;">
                                    <input type="text" class="form-control" name="search" value="{{isset($search) ? $search : ''}}" placeholder="Search by MSO No. or user name,email or phone" style="margin-top: 1px;"/>
                                <span class="input-group-addon btn btn-default">
                                    <button class="btn btn-default">Search now</button>
                                </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4 col-xs-4">
                                <div class="input-group">
                                    <select class="form-control select" id="company_id" name="company_id">
                                        <option selected disabled>Select Company</option>
                                        @foreach($companies as $company)
                                            <option value="{{$company->id}}">{{$company->en_name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <select class="form-control" id="sub_company" name="sub_company">
                                <option selected disabled>Select Company first </option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 {{ $errors->has('from') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">From </label>
                            <div class="input-group">
                                <input type="date" class="form-control" name="from" value="">
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            </div>
                            @include('admin.layouts.error', ['input' => 'from'])
                        </div>

                        <div class="form-group col-md-4 {{ $errors->has('to') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">To </label>
                            <div class="input-group">
                                <input type="date" class="form-control" name="to" value="">
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            </div>
                            @include('admin.layouts.error', ['input' => 'to'])

                        </div>
                        {{--@if(isset($cats))--}}
                        <div class="form-group col-md-2">
                            <select class="form-control select" id="main_cats" name="main_cats" required>
                                <option selected disabled>Select A Main Category</option>
                                @foreach($cats as $cat)
                                    <option value="{{$cat->id}}">{{$cat->en_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <select class="form-control" name="sub_cats" data-style="btn-success" id="sub_cats">
                                <option selected disabled>Select A Category First</option>
                            </select>
                        </div>
                    </form>

                     <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>MSO No.</th>
                                    <th>type</th>
                                    <th>Badge ID</th>
                                    <th>User</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Items</th>
                                    <th>Operations</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <td>{{$order->id}}</td>
                                        <td>{{isset($order->smo) ? $order->smo : '-'}}</td>
                                        <td>{{$order->type}}</td>
                                        <td>{{$order->user->badge_id}}</td>
                                        <td>{{$order->user->en_name}}</td>
                                        <td>
                                            @if($order->type == 'urgent')
                                                {{$order->created_at}}
                                            @elseif($order->type == 'scheduled')
                                                {{$order->scheduled_at}}
                                            @else
                                                {{isset($order->scheduled_at) ? $order->scheduled_at : 'Not selected yet'}}
                                            @endif
                                        </td>
                                        <td>@if($order->completed == 1) <span class="label label-success">Completed</span> @elseif($order->canceled == 1) @if($order->canceled_by == 'user') <span class="label label-danger">Canceled By User</span> @else <span class="label label-danger">Canceled By Technician</span> @endif @else <span class="label label-primary">Open</span> @endif</td>
                                        <td>{{$order->items->count()}}</td>
                                        <td>
                                            <a title="View" href="/company/order/{{$order->id}}/view"><button class="btn btn-info btn-condensed"><i class="fa fa-eye"></i></button></a>
                                            {{--<button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$order->id}}" title="Delete"><i class="fa fa-trash-o"></i></button>--}}
                                        </td>
                                    </tr>

                                    {{--<!-- danger with sound -->--}}
                                    {{--<div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-warning-{{$order->id}}">--}}
                                        {{--<div class="mb-container">--}}
                                            {{--<div class="mb-middle warning-msg alert-msg">--}}
                                                {{--<div class="mb-title"><span class="fa fa-times"></span>Alert !</div>--}}
                                                {{--<div class="mb-content">--}}
                                                    {{--<p>Your are about to delete a warehouse item,and you won't be able to restore its data again .</p>--}}
                                                    {{--<br/>--}}
                                                    {{--<p>Are you sure ?</p>--}}
                                                {{--</div>--}}
                                                {{--<div class="mb-footer buttons">--}}
                                                    {{--<button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">Close</button>--}}
                                                    {{--<form method="post" action="/company/warehouse/item/delete" class="buttons">--}}
                                                        {{--{{csrf_field()}}--}}
                                                        {{--<input type="hidden" name="item_code" value="{{$order->code}}">--}}
                                                        {{--<button type="submit" class="btn btn-danger btn-lg pull-right">Delete</button>--}}
                                                    {{--</form>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<!-- end danger with sound -->--}}
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#company_id').on('change', function (e) {
            var parent_id = $('#company_id').val();
            if (parent_id) {
                $.ajax({
                    url: '/provider/get_sub_company/'+parent_id,
                    type: "GET",

                    dataType: "json",

                    success: function (data) {
                        $('#sub_company').empty();
                        $('#sub_company').append('<option selected disabled> Select a Sub Company </option>');
                        $.each(data, function (i, sub_company) {
                            $('#sub_company').append('<option value="' + sub_company.id + '">' + sub_company.en_name + '</option>');
                        });
                    }
                });

            }
        });

        $('#main_cats').on('change', function (e) {
            var parent_id = e.target.value;
            var company_id = $('#company_id').val();
            if (parent_id) {
                $.ajax({
                    url: '/provider/get_sub_category_provider/'+parent_id,
                    type: "GET",

                    dataType: "json",

                    success: function (data) {
                        $('#sub_cats').empty();
                        $('#sub_cats').append('<option selected disabled> Select a Sub Category </option>');
                        $.each(data, function (i, sub_cat) {
                            $('#sub_cats').append('<option value="' + sub_cat.id + '">' + sub_cat.en_name + '</option>');
                        });
                    }
                });

            }
        });
    </script>
@endsection
