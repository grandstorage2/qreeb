@extends('provider.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/provider/dashboard">Dashboard</a></li>
        <li>Orders</li>
        <li class="active">{{$type}}</li>
    </ul>
    <!-- END BREADCRUMB -->

    <style>
        .image
        {
            height: 50px;
            width: 50px;
            border: 1px solid #29B2E1;
            border-radius: 100px;
            box-shadow: 2px 2px 2px darkcyan;
        }
    </style>
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('provider.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{--<a href="/provider/orders/excel/view"><button type="button" class="btn btn-info"> Upload excel tech details </button></a>--}}
                        <a href="/provider/orders/excel/tech/view"><button type="button" class="btn btn-info"> Upload excel tech details and items </button></a>
                        {{--<a href="/provider/orders/images/view"><button type="button" class="btn btn-info"> Upload images compressed file </button></a>--}}

                        <a href="/provider/orders/{{$type}}/invoice/request" style="float: right;"><button type="button" class="btn btn-success"> Info Sheet Request <i class="fa fa-file-excel-o"></i></button></a>
                    </div>

                    <form class="form-horizontal" method="get" action="/provider/orders/{{$type}}/search">
                        @include('provider.orders_search')
                    </form>

                    {{--<form class="form-horizontal" method="get" @if(Request::is('provider/orders/urgent')) action="/provider/orders/urgent/search"--}}
                    {{--@elseif(Request::is('provider/orders/scheduled')) action="/provider/orders/scheduled/search"--}}
                    {{--@elseif (Request::is('provider/orders/re_scheduled')) action="/provider/orders/re_scheduled/search"--}}
                    {{--@else action="/provider/orders/canceled/search" @endif>--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-xs-12">--}}
                                {{--<div class="input-group" style="margin-top: 10px;">--}}
                                    {{--<input type="text" class="form-control" name="search" value="{{isset($search) ? $search : ''}}" placeholder="Search by SMO No. or technician badge_id,name,email or phone" style="margin-top: 1px;"/>--}}
                                    {{--<span class="input-group-addon btn btn-default">--}}
                                            {{--<button class="btn btn-default">Search now</button>--}}
                                    {{--</span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                     <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Order No.</th>
                                    <th>MSO No.</th>
                                    <th>type</th>
                                    {{--<th>Badge ID</th>--}}
                                    <th>Technician</th>
                                    <th>User</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    {{--<th>Items</th>--}}
                                    <th>Operations</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <td>{{isset($order->id) ? $order->id : '-'}}</td>
                                        <td>{{isset($order->smo) ? $order->smo : '-'}}</td>
                                        <td>{{$type}}</td>
                                        {{--<td>@if(isset($order->tech_id))  {{$order->tech->badge_id}} @else Not selected yet @endif</td>--}}
                                        <td>{{isset($order->tech_id) ? $order->tech->en_name : 'Not selected yet'}}</td>
                                        <td>{{$order->user->en_name}}</td>
                                        <td>
                                            @if($order->type == 'urgent')
                                                {{$order->created_at}}
                                            @elseif($order->type == 'scheduled')
                                                {{$order->scheduled_at}}
                                            @else
                                                {{isset($order->scheduled_at) ? $order->scheduled_at : 'Not selected yet'}}
                                            @endif
                                        </td>
                                        <td>@if($order->completed == 1 && $order->canceled == 0) <span class="label label-success">Completed</span> @elseif($order->completed == 0 && $order->canceled == 1) @if($order->canceled_by == 'user') <span class="label label-danger">Canceled By User</span> @elseif($order->canceled_by == 'tech') <span class="label label-danger">Canceled By Technician</span> @else <span class="label label-danger">Canceled By Admin</span> @endif @else <span class="label label-primary">Open</span> @endif</td>
                                        {{--<td>{{$order->items->count()}}</td>--}}
                                        <td>
                                            <a title="View" href="/provider/order/{{$order->id}}/view"><button class="btn btn-info btn-condensed"><i class="fa fa-eye"></i></button></a>
                                            @if($order->completed == 0 && $order->canceled == 0)
                                            <button class="btn btn-warning btn-condensed mb-control" data-box="#message-box-danger-{{$order->id}}" title="Cancel"><i class="fa fa-times-circle"></i></button>
                                            @endif
                                            {{--<a data-toggle="modal" data-target="#modal_update_{{$order->id}}" title="تعديل" class="buttons"><button class="btn btn-info btn-condensed"><i class="fa fa-edit"></i></button></a>--}}
                                            {{--<input type="hidden" name="order_id" value="{{$order->id}}" />--}}
                                            {{--<button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$order->id}}" title="Delete"><i class="fa fa-trash-o"></i></button>--}}
                                        </td>
                                    </tr>

                                    <!-- danger with sound -->

                                    <div class="message-box message-box-warning animated fadeIn" data-sound="alert/fail" id="message-box-danger-{{$order->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span> Alert !</div>
                                                <div class="mb-content">
                                                    <p>Your are about to cancel order,are you sure? .</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <form method="post" action="/provider/order/cancel/{{$order->type}}" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="order_id" value="{{$order->id}}">
                                                        <button class="btn btn-warning btn-lg btn-warning btn-lg pull-right">Cancel</button>
                                                    </form>
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end danger with sound -->

                                    {{--<div class="modal animated fadeIn" id="modal_update_{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">--}}
                                        {{--<div class="modal-dialog">--}}
                                            {{--<div class="modal-content">--}}
                                                {{--<div class="modal-header">--}}
                                                    {{--<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">إغلاق</span></button>--}}
                                                    {{--<h4 class="modal-title" id="smallModalHead">edit order</h4>--}}
                                                {{--</div>--}}
                                                {{--<form method="post" action="/provider/order/edit" enctype="multipart/form-data">--}}
                                                    {{--{{csrf_field()}}--}}
                                                    {{--<div class="modal-body form-horizontal form-group-separated">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label class="col-md-3 control-label">items</label>--}}
                                                            {{--<div class="col-md-9">--}}
                                                                {{--@foreach($items as $item)--}}
                                                                    {{--<p>{{$item->en_name}} - {{$item->en_desc}}</p>--}}
                                                                {{--@endforeach--}}
                                                                {{--@include('admin.layouts.error', ['input' => 'password'])--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<input type="hidden" name="order_id" value="{{$order->id}}" />--}}
                                                    {{--<div class="modal-footer">--}}
                                                        {{--<button type="submit" class="btn btn-success">تعديل</button>--}}
                                                    {{--</div>--}}
                                                {{--</form>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                @endforeach

                                </tbody>
                            </table>


                            {{$orders->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#company_id').on('change', function (e) {
            var parent_id = $('#company_id').val();
            if (parent_id) {
                $.ajax({
                    url: '/provider/get_sub_company/'+parent_id,
                    type: "GET",

                    dataType: "json",

                    success: function (data) {
                        $('#sub_company').empty();
                        $('#sub_company').append('<option selected disabled> Select a Sub Company </option>');
                        $.each(data, function (i, sub_company) {
                            $('#sub_company').append('<option value="' + sub_company.id + '">' + sub_company.en_name + '</option>');
                        });
                    }
                });

            }
        });

        $('#main_cats').on('change', function (e) {
            var parent_id = e.target.value;
            var company_id = $('#company_id').val();
            if (parent_id) {
                $.ajax({
                    url: '/provider/get_sub_category_provider/'+parent_id,
                    type: "GET",

                    dataType: "json",

                    success: function (data) {
                        $('#sub_cats').empty();
                        $('#sub_cats').append('<option selected disabled> Select a Sub Category </option>');
                        $.each(data, function (i, sub_cat) {
                            $('#sub_cats').append('<option value="' + sub_cat.id + '">' + sub_cat.en_name + '</option>');
                        });
                    }
                });

            }
        });
    </script>
@endsection
