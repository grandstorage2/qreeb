@extends('provider.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/provider/dashboard">Dashboard</a></li>
        <li>Orders</li>
        <li class="active">waiting</li>
    </ul>
    <!-- END BREADCRUMB -->

    <style>
        .image
        {
            height: 50px;
            width: 50px;
            border: 1px solid #29B2E1;
            border-radius: 100px;
            box-shadow: 2px 2px 2px darkcyan;
        }
    </style>
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('provider.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/provider/orders/open/waiting/upload/view"><button type="button" class="btn btn-info"> Upload excel tech details </button></a>
                        {{--<a href="/provider/orders/images/view"><button type="button" class="btn btn-info"> Upload images compressed file </button></a>--}}

                    </div>


                    <div class="panel-body">
                        @foreach($shows as $show)
                            @if(strpos($show, \Carbon\Carbon::now()->format('Y-m-d')))
                            <h2>New export file</h2>
                            <a href="{{$show}}" style="padding: 15px;"><button type="button" class="btn btn-success" style="font-size: 18px"> export <i style="font-size: 14px" class="fa fa-file-excel-o"></i></button></a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
