@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">Dashboard</a></li>
        <li class="active">Bills</li>
    </ul>
    <!-- END BREADCRUMB -->

    <style>
        .image
        {
            height: 50px;
            width: 50px;
            border: 1px solid #29B2E1;
            border-radius: 100px;
            box-shadow: 2px 2px 2px darkcyan;
        }
    </style>
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('provider.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div>
                            {{isset($from)?$from:''}} &nbsp;
                            {{isset($to)?$to:''}} &nbsp;
                            {{isset($sub_company)?$sub_company->en_name:''}} &nbsp;
                            {{isset($main_cats)?$main_cats->en_name:''}} &nbsp;
                            {{isset($sub_cats)?$sub_cats->en_name:''}}
                        </div>

                        @if(Request::is("admin/provider/$id/bills"))
                            <a href="/admin/provider/{{$id}}/bills_export" style="float: right;">
                                <button type="button" class="btn btn-success"> Export Orders <i class="fa fa-file-excel-o"></i> </button>
                            </a>
                        @else
                            <a href="/provider/collaboration/{{$id}}/bills_export/{{$search}}" style="float: right;">
                                <button type="button" class="btn btn-success"> Export Orders <i class="fa fa-file-excel-o"></i> </button>
                            </a>
                            {{--<a title="View bills" style="float: right; padding-right: 10px" href=""><button class="btn btn-primary btn-condensed">Bills </button></a>--}}
                            <form method="post" action="/admin/provider/{{$id}}/bills/view" style="float: right; padding-right: 10px">
                                {{csrf_field()}}
                                <button class="btn btn-primary btn-condensed"><i class="fa fa-money"></i> Bills</button>
                                <input type="hidden" name="order_data" value="{{$bills_orders}}">
                            </form>
                        @endif
                        {{--<a href="/provider/collaboration/{{$id}}/bills_export" style="float: right; margin-right: 3px;">--}}
                        {{--@foreach($orders as $value)--}}
                        {{--<input type="hidden" name="orders" value="{{$value}}">--}}
                        {{--@endforeach--}}
                    </div>

                    <form class="form-horizontal" method="get" action="/admin/provider/{{$id}}/bills/search">
                        <div class="form-group">
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group" style="margin-top: 10px;">
                                    <input type="text" class="form-control" name="search" value="{{isset($search) ? $search : ''}}" placeholder="Search by SMO No. or Order No. or Technician name or User name" style="margin-top: 1px;"/>
                                    <span class="input-group-addon btn btn-default">
                                            <button class="btn btn-default">Search now</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-xs-4">
                                <div class="input-group">
                                    <select class="form-control select" id="company_id" name="company_id">
                                    <option selected disabled>Select Company</option>
                                    @foreach($companies as $company)
                                    <option value="{{$company->id}}">{{$company->en_name}}</option>
                                    @endforeach
                                    </select>
                                    <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <select class="form-control" id="sub_company" name="sub_company[]" multiple>
                                <option selected disabled>Select Company first </option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 {{ $errors->has('from') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">From </label>
                            <div class="input-group">
                                <input type="date" class="form-control" name="from" value="">
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            </div>
                            @include('admin.layouts.error', ['input' => 'from'])
                        </div>

                        <div class="form-group col-md-4 {{ $errors->has('to') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label">To </label>
                            <div class="input-group">
                                <input type="date" class="form-control" name="to" value="">
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            </div>
                            @include('admin.layouts.error', ['input' => 'to'])
                        </div>
                        <div class="form-group col-md-3">
                            <select class="form-control select" id="main_cats" name="main_cats" required>
                                <option selected disabled>Select A Main Category</option>
                                @foreach($cats as $cat)
                                    <option value="{{$cat->id}}">{{$cat->en_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <select class="form-control" name="sub_cats" data-style="btn-success" id="sub_cats">
                                <option selected disabled>Select A Category First</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <select class="form-control select" name="service_type[]" multiple>
                                <option selected disabled>Select service type</option>
                                <option value="1">Preview</option>
                                <option value="2">Maintenance</option>
                                <option value="3">Structure</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10">
                                <p>Price Range</p>
                                <input type="text" id="ise_step" name="price_range" value="" />
                            </div>
                        </div>
                    </form>
                    <p style="float:right; padding-right: 20px">count: {{count($orders)}}</p>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Order No.</th>
                                    <th>MSO No.</th>
                                    <th>type</th>
                                    <th>User</th>
                                    <th>Technician</th>
                                    <th>Status</th>
                                    <th>Service Fee</th>
                                    <th>Items Total</th>
                                    <th>Total Amount</th>
                                    <th>Date</th>
                                    {{--<th>Operations</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <td>{{isset($order->id) ? $order->id : '-'}}</td>
                                        <td>{{isset($order->smo) ? $order->smo : '-'}}</td>
                                        <td>{{$order->type}}</td>
                                        <td>{{isset($order->user_id) ? $order->user->en_name : 'Not selected yet'}}</td>
                                        <td>{{isset($order->tech_id) ? $order->tech->en_name : 'Not selected yet'}}</td>
                                        <td>@if($order->completed == 1 && $order->canceled == 0) <span class="label label-success">Completed</span> @elseif($order->completed == 0 && $order->canceled == 1) @if($order->canceled_by == 'user') <span class="label label-danger">Canceled By User</span> @elseif($order->canceled_by == 'tech') <span class="label label-danger">Canceled By Technician</span> @else <span class="label label-danger">Canceled By Admin</span> @endif @else <span class="label label-primary">Open</span> @endif</td>
                                        <td>{{$order->order_total}} S.R</td>
                                        <td>{{$order->item_total}} S.R</td>
                                        <td>{{$order->item_total+$order->order_total}} S.R</td>
                                        <td>{{$order->created_at}}</td>
                                        <td>
                                            <a title="View" href="/provider/order/{{$order->id}}/view"><button class="btn btn-info btn-condensed"><i class="fa fa-eye"></i></button></a>
                                            {{--<a title="View bills" href="/provider/collaboration/{{$id}}/bills/{{$order->id}}/view"><button class="btn btn-primary btn-condensed"><i class="fa fa-money"></i></button></a>--}}
                                        </td>
                                        {{--<td>--}}
                                        {{--<a title="View" href="/providerprovider/order/{{$order->id}}/view"><button class="btn btn-info btn-condensed"><i class="fa fa-eye"></i></button></a>--}}
                                        {{--<button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$order->id}}" title="Delete"><i class="fa fa-trash-o"></i></button>--}}
                                        {{--</td>--}}
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
                {{$orders->links()}}
            </div>
        </div>
    </div>
    <script>

        // $(document).ready(function (e) {
        //     var parent_id = $('#company_id').val();
        //     if (parent_id) {
        //         $.ajax({
        //             url: '/provider/get_sub_company/'+parent_id,
        //             type: "GET",
        //
        //             dataType: "json",
        //
        //             success: function (data) {
        //                 $('#sub_company').empty();
        //                 $('#sub_company').append('<option selected disabled> Select a Sub Company </option>');
        //                 $.each(data, function (i, sub_company) {
        //                     $('#sub_company').append('<option value="' + sub_company.id + '">' + sub_company.en_name + '</option>');
        //                 });
        //             }
        //         });
        //
        //     }
        // });

        $('#company_id').on('change', function (e) {
            var parent_id = $('#company_id').val();
            if (parent_id) {
                $.ajax({
                    url: '/admin/provider/get_sub_company/'+parent_id,
                    type: "GET",

                    dataType: "json",

                    success: function (data) {
                        $('#sub_company').empty();
                        $('#sub_company').append('<option selected disabled> Select a Sub Company </option>');
                        $.each(data, function (i, sub_company) {
                            $('#sub_company').append('<option value="' + sub_company.id + '">' + sub_company.en_name + '</option>');
                        });
                    }
                });

            }
        });

        $('#main_cats').on('change', function (e) {
            var parent_id = e.target.value;
            var company_id = $('#company_id').val();
            if (parent_id) {
                $.ajax({
                    url: '/admin/provider/'+company_id+'/get_sub_category_provider/'+parent_id,
                    type: "GET",

                    dataType: "json",

                    success: function (data) {
                        $('#sub_cats').empty();
                        $('#sub_cats').append('<option selected disabled> Select a Sub Category </option>');
                        $.each(data, function (i, sub_cat) {
                            $('#sub_cats').append('<option value="' + sub_cat.id + '">' + sub_cat.en_name + '</option>');
                        });
                    }
                });

            }
        });

    </script>
@endsection
