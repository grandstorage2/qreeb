@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">Dashboard</a></li>
        <li>Orders</li>
        <li class="active">View Order</li>
    </ul>
    <!-- END BREADCRUMB -->

    <!-- PAGE CONTENT WRAPPER -->
    <form class="form-horizontal" method="get" action="/admin/provider/bills/all/search">

        <div class="form-group col-md-4 {{ $errors->has('from') ? ' has-error' : '' }}">
            <label class="col-md-2 control-label">From </label>
            <div class="input-group">
                <input type="date" class="form-control" name="from" value="{{isset($from) ? $from : ''}}" required>
                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
            </div>
        </div>

        <div class="form-group col-md-4 {{ $errors->has('to') ? ' has-error' : '' }}">
            <label class="col-md-2 control-label">To </label>
            <div class="input-group">
                <input type="date" class="form-control" name="to" value="{{isset($to) ? $to : ''}}" required>
                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
            </div>
        </div>

        <div class="form-group col-md-4">
            <div class="input-group">
                <button class="btn btn-default">Search now</button>
            </div>
        </div>

    </form>

    @foreach($providers as $provider)
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-striped sticky-header">
                            <thead>
                            <tr id="myHeader">
                                <th class="text-center">Name</th>
                                <th class="text-center">Count orders</th>
                                <th class="text-center">Total orders</th>
                                <th class="text-center">Total items</th>
                                <th class="text-center">Total</th>
                                <th class="text-center">Interest fee</th>
                                <th class="text-center">Warehouse fee</th>
                                <th class="text-center">Total Material Cost</th>
                            </tr>
                            </thead>

                            <tbody>
                                @if(isset($from) && isset($to))
                                    <tr>
                                        <td class="text-center">{{$provider->en_name}}</td>
                                        <td class="text-center">{{$provider->orders_with_date($from,$to)->count()}}</td>
                                        <td class="text-center">{{$provider->orders_with_date($from,$to)->sum('order_total')}}</td>
                                        <td class="text-center">{{$provider->orders_with_date($from,$to)->sum('item_total')}}</td>
                                        <td class="text-center">{{$provider->orders_with_date($from,$to)->sum('order_total') + $provider->orders_with_date($from,$to)->sum('item_total')}}</td>
                                        @if($provider->type == 'percentage')
                                            <td class="text-center">{{ ($provider->orders_with_date($from,$to)->sum('order_total') * $provider->interest_fee) / 100 }} S.R</td>
                                            <td class="text-center">{{ ($provider->orders_with_date($from,$to)->sum('item_total') * $provider->warehouse_fee) / 100 }} S.R</td>
                                            <td class="text-center">{{ $provider->orders_with_date($from,$to)->sum('order_total') + ($provider->orders_with_date($from,$to)->sum('order_total') * $provider->interest_fee)/100 + ($provider->orders_with_date($from,$to)->sum('item_total') * $provider->warehouse_fee)/100}} S.R</td>
                                        @else
                                            <td class="text-center">{{$provider->orders_with_date($from,$to)->sum('order_total') - $provider->interest_fee}} S.R</td>
                                            <td class="text-center">{{$provider->orders_with_date($from,$to)->sum('item_total') - $provider->warehouse_fee}} S.R</td>
                                            <td class="text-center">{{$provider->orders_with_date($from,$to)->sum('order_total') + ($provider->orders_with_date($from,$to)->sum('order_total') - $provider->interest_fee) + ($provider->orders_with_date($from,$to)->sum('item_total') - $provider->warehouse_fee)}} S.R</td>

                                        @endif

                                    </tr>
                                @else
                                    <tr>
                                        <td class="text-center">{{$provider->en_name}}</td>
                                        <td class="text-center">{{$provider->orders->count()}}</td>
                                        <td class="text-center">{{$provider->orders->sum('order_total')}}</td>
                                        <td class="text-center">{{$provider->orders->sum('item_total')}}</td>
                                        <td class="text-center">{{$provider->orders->sum('order_total') + $provider->orders->sum('item_total')}}</td>
                                        @if($provider->type == 'percentage')
                                            <td class="text-center">{{ ($provider->orders->sum('order_total') * $provider->interest_fee) / 100 }} S.R</td>
                                            <td class="text-center">{{ ($provider->orders->sum('item_total') * $provider->warehouse_fee) / 100 }} S.R</td>
                                            <td class="text-center">{{ $provider->orders->sum('order_total') + ($provider->orders->sum('order_total') * $provider->interest_fee)/100 + ($provider->orders->sum('item_total') * $provider->warehouse_fee)/100}} S.R</td>
                                        @else
                                            <td class="text-center">{{$provider->orders->sum('order_total') - $provider->interest_fee}} S.R</td>
                                            <td class="text-center">{{$provider->orders->sum('item_total') - $provider->warehouse_fee}} S.R</td>
                                            <td class="text-center">{{$provider->orders->sum('order_total') + ($provider->orders->sum('order_total') - $provider->interest_fee) + ($provider->orders->sum('item_total') - $provider->warehouse_fee)}} S.R</td>

                                        @endif

                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
        <!-- END PAGE CONTENT WRAPPER -->

@endsection
