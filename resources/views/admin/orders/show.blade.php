@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">Dashboard</a></li>
        <li>Orders</li>
        <li class="active">View Order</li>
    </ul>
    <!-- END BREADCRUMB -->

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-body">
                        <a href="/admin/order/{{$order->id}}/view" class="btnprn pull-right" style="font-size: 20px"> <i class="fa fa-print"></i> print</a>
                        <script>
                            $(document).ready(function () {
                                $('.btnprn').printPage();
                            });
                        </script>
                        <h2>Order<strong> #{{$order->id}}</strong></h2>
                        @if(isset($order->smo))
                            <h2>MSO No.<strong> #{{$order->smo}}</strong></h2>
                        @endif


                        <h2>Category : {{$order->category->parent->en_name}} - {{$order->category->en_name}} {{$order->created_at}} ({{$order->created_at->diffForHumans()}})</h2>

                    {{--<div class="push-down-10 pull-right">--}}
                    {{--<button class="btn btn-default"><span class="fa fa-print"></span> Print</button>--}}
                    {{--</div>--}}
                    <!-- INVOICE -->
                        <div class="invoice">

                            <div class="row">
                                <div class="col-md-4">

                                    <div class="invoice-address">
                                        <h5>User</h5>
                                        <h6>Company: {{$order->user->company->en_name}}</h6>
                                        <p>name: {{$order->user->en_name}}</p>
                                        <p>Phone: {{$order->user->phone}}</p>
                                        <p>Id: {{$order->user->badge_id}}</p>
                                        @foreach($order->get_user_location_admin($order->user_id) as $key => $value)
                                            <p>
                                                {{$key}} : {{$value}}
                                            </p>
                                        @endforeach
                                        <p>Count orders: {{$count}}</p>
                                    </div>

                                </div>
                                <div class="col-md-4">

                                    <div class="invoice-address">
                                        <h5>Technician</h5>
                                        @if(isset($order->tech_id))
                                            <h6>Provider: {{$order->tech->provider->en_name}}</h6>
                                            <p>name: <a href="/provider/technician/{{$order->tech_id}}/view"> {{$order->tech->en_name}}</a></p>
                                            <p>Phone: {{$order->tech->phone}}</p>
                                        @else
                                            <h6>Not selected yet</h6>
                                        @endif
                                    </div>

                                </div>
                                <div class="col-md-4">

                                    <div class="invoice-address">
                                        <h5>Tracking</h5>
                                        @foreach($order->get_steps('en',$order->id) as $get_steps)
                                            @if($get_steps['flag'] != 0)
                                                <p>{{$get_steps['text']}}</p>
                                            @endif
                                        @endforeach
                                    </div>

                                    {{--<div class="invoice-address">--}}
                                    {{--<h5>Invoice</h5>--}}
                                    {{--<table class="table table-striped">--}}
                                    {{--<tr>--}}
                                    {{--<td>Created at: </td><td class="text-right">{{$order->created_at}}</td>--}}
                                    {{--</tr>--}}

                                    {{--@if($order->scheduled_at != NULL)--}}
                                    {{--<tr>--}}
                                    {{--<td>Scheduled at: </td><td class="text-right">{{$order->scheduled_at}}</td>--}}
                                    {{--</tr>--}}
                                    {{--@endif--}}

                                    {{--@if(isset($order->provider_id))--}}
                                    {{--<tr>--}}
                                    {{--<td><strong>Service Fee:</strong></td><td class="text-right"><strong>{{$order->get_fee($order->provider_id,$order->cat_id)}}</strong></td>--}}
                                    {{--</tr>--}}
                                    {{--@endif--}}
                                    {{--</table>--}}

                                    {{--</div>--}}

                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-4">
                                    <div class="invoice-address">
                                        <h5>User Extra Details</h5>
                                        @if(isset($order->user_details->place))
                                            <h6>Place</h6>
                                            <p>{{$order->user_details->place}}</p>
                                        @endif
                                        @if(isset($order->user_details->place))
                                            <h6>Part</h6>
                                            <p>{{$order->user_details->part}}</p>
                                        @endif
                                        @if(isset($order->user_details->place))
                                            <h6>Description</h6>
                                            <p>{{$order->user_details->desc}}</p>
                                        @endif
                                        @if(isset($order->user_details->images))
                                            <div class="gallery">
                                                @foreach(unserialize($order->user_details->images) as $image)
                                                    <a class="gallery" href="/orders/{{$image}}" title="/orders/{{$image}}" data-gallery>
                                                        <div class="image">
                                                            <img  style="width: 200px; height: 200px; display: table; margin: 0 auto;" src="/orders/{{$image}}" alt="{{$image}}"/>
                                                        </div>
                                                    </a>
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                @if(isset($order->tech_id) && $order->tech_details)
                                    <div class="col-md-6">
                                        <div class="invoice-address">
                                            <h5>Tech Extra Details</h5>

                                            {{--<p>{{$order->tech_details->category->parent->en_name}} - {{$order->tech_details->category->parent->en_name}} - {{$order->tech_details->category->en_name}}</p>--}}

                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th class="rtl_th">Problem Type</th>
                                                    <th class="rtl_th">Price</th>
                                                    <th class="rtl_th">Working hours</th>
                                                    <th class="rtl_th">Description</th>
                                                    <th class="rtl_th">Multiply</th>
                                                </tr>
                                                </thead>
                                                @foreach($tech_details as $tech_detail)
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            {{$tech_detail->category->en_name}}
                                                        </td>
                                                        <td>
                                                            {{isset($tech_detail->category->cat_fee($order->company_id)->third_fee) ?
                                                            $tech_detail->category->cat_fee($order->company_id)->third_fee : 0}}
                                                        </td>
                                                        <td>
                                                            {{$tech_detail->working_hours}}
                                                        </td>
                                                        <td>{{$tech_detail->desc}}</td>
                                                        @if(isset($tech_detail->category->cat_fee($order->company_id)->third_fee))
                                                            <td>{{$tech_detail->working_hours * $tech_detail->category->cat_fee($order->company_id)->third_fee }}</td>
                                                        @else
                                                            <td>{{$tech_detail->working_hours * 0}}</td>
                                                        @endif
                                                    </tr>
                                                    </tbody>
                                                    <div class="col-md-5">
                                                        @if(isset($tech_detail->before_images))
                                                            <h6>Before Maintenance</h6>
                                                            <div class="gallery">
                                                                @foreach(unserialize($tech_detail->before_images) as $image)
                                                                    <a target="_blank" href="/orders/{{$image}}" title="/orders/{{$image}}" data-gallery>
                                                                        <div class="image">
                                                                            <img  style="width: 200px; height: 200px; display: table; margin: 0 auto;" src="/orders/{{$image}}" alt="{{$image}}"/>
                                                                        </div>
                                                                    </a>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-5">
                                                        @if(isset($tech_detail->after_images))
                                                            <h6>After Maintenance</h6>
                                                            <div class="gallery">
                                                                @foreach(unserialize($tech_detail->after_images) as $image)
                                                                    <a target="_blank" href="/orders/{{$image}}" title="/orders/{{$image}}" data-gallery>
                                                                        <div class="image">
                                                                            <img  style="width: 200px; height: 200px; display: table; margin: 0 auto;" src="/orders/{{$image}}" alt="{{$image}}"/>
                                                                        </div>
                                                                    </a>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    </div>
                                                @endforeach
                                            </table>




                                        </div>
                                    </div>
                                @endif
                            </div>

                            <!-- BLUEIMP GALLERY -->
                            <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>
                            <!-- END BLUEIMP GALLERY -->

                            @if(isset($order->items))
                                <div class="table-invoice">
                                    <table class="table">
                                        <tr>
                                            <th>Item Description</th>
                                            <th class="text-center">Item Price</th>
                                            <th class="text-center">Item Count</th>
                                            <th class="text-center">Image</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Total</th>
                                        </tr>
                                        @foreach($order->items as $item)
                                            <tr>
                                                <td>
                                                    <div><strong>{{$item->get_this_item($item->provider_id,$item->item_id)->en_name}}</strong>
                                                        <p>{{$item->get_this_item($item->provider_id,$item->item_id)->en_desc}}</p>
                                                    </div>
                                                </td>
                                                <td class="text-center">{{$item->get_this_item($item->provider_id,$item->item_id)->price}} S.R</td>
                                                <td class="text-center">{{$item->taken}}</td>
                                                <td class="text-center"><a target="_blank" href="/warehouses/{{$item->get_this_item($item->provider_id,$item->item_id)->image}}" title="/warehouses/{{$item->get_this_item($item->provider_id,$item->item_id)->image}}" data-gallery>
                                                        <img src="/warehouses/{{$item->get_this_item($item->provider_id,$item->item_id)->image}}" class="image_radius"/></a></td>
                                                <td class="text-center">@if($item->status == 'confirmed') <span class="label label-success">Approved</span> @elseif($item->status == 'awaiting') <span class="label label-warning">Awaiting</span> @else <span class="label label-danger">Declined</span> @endif</td>

                                            </tr>
                                        @endforeach
                                        <th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-center">{{$order->item_total}} S.R</td>
                                        </th>
                                    </table>
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-md-6">
                                    <h4>Amount Due</h4>

                                    <table class="table table-striped">
                                        <tr>
                                            <td width="200"><strong>Service Fee:</strong></td><td class="text-right">{{$order->order_total}} S.R</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Items Total:</strong></td><td class="text-right">{{$order->item_total}} S.R</td>
                                        </tr>
                                        <tr class="total">
                                            <td>Total Amount:</td><td class="text-right">{{$order->order_total + $order->item_total}} S.R</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END INVOICE -->

                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->

    <script>
        document.getElementById('links').onclick = function (event) {
            event = event || window.event;
            var target = event.target || event.srcElement;
            var link = target.src ? target.parentNode : target;
            var options = {index: link, event: event,onclosed: function(){
                    setTimeout(function(){
                        $("body").css("overflow","");
                    },200);
                }};
            var links = this.getElementsByTagName('a');
            blueimp.Gallery(links, options);
        };
    </script>

@endsection
